package org.campagnelab.allogenomics.scoring;

import org.junit.Test;

import static junit.framework.Assert.assertEquals;

/**
 * @author Fabien Campagne
 *         Date: 6/5/13
 *         Time: 2:50 PM
 */
public class AlloGenomicsScoresTest {
    @Test
    public void testObserveGenotypeMismatch() throws Exception {
        AlloGenomicsScores scorerDefault = new AlloGenomicsScores(AlloGenomicsScores.TypeOfScore.GLOBAL);
        scorerDefault.setOnlyHomozygoteAlleles(false);
        assertEquals(1, scorerDefault.observeGenotypeMismatch("A", "C", "0/0", "1/1", null, null));
        assertEquals(0, scorerDefault.observeGenotypeMismatch("A", "C", "0/0", "0/1", null, null));
        assertEquals(1, scorerDefault.observeGenotypeMismatch("A", "C", "0/1", "0/0", null, null));
        assertEquals(0, scorerDefault.observeGenotypeMismatch("A", "", "0/0", "0/0", null, null));
        assertEquals(0, scorerDefault.observeGenotypeMismatch("C", "", "0/0", "0/0", null, null));
        assertEquals(0, scorerDefault.observeGenotypeMismatch("A", "C", "0/0", "0/0", null, null));
        assertEquals(2, scorerDefault.observeGenotypeMismatch("A", "C,T", "1/2", "0/0", null, null));


        scorerDefault.setOnlyHomozygoteAlleles(true);
        assertEquals(1, scorerDefault.observeGenotypeMismatch("A", "C", "0/0", "1/1", null, null));
        assertEquals(1, scorerDefault.observeGenotypeMismatch("A", "C", "1/1", "0/0", null, null));

        assertEquals(0, scorerDefault.observeGenotypeMismatch("A", "C", "0/0", "0/1", null, null));
        assertEquals(0, scorerDefault.observeGenotypeMismatch("A", "C", "0/1", "0/0", null, null));
        assertEquals(0, scorerDefault.observeGenotypeMismatch("A", "", "0/0", "0/0", null, null));
        assertEquals(0, scorerDefault.observeGenotypeMismatch("C", "", "0/0", "0/0", null, null));
        assertEquals(0, scorerDefault.observeGenotypeMismatch("A", "C", "0/0", "0/0", null, null));
        assertEquals(0, scorerDefault.observeGenotypeMismatch("A", "C,T", "1/2", "0/0", null, null));

    }

    @Test
    public void testObserveSynonymous() throws Exception {
        AlloGenomicsScores scorerDefault = new AlloGenomicsScores(AlloGenomicsScores.TypeOfScore.GLOBAL);
        scorerDefault.setOnlyHomozygoteAlleles(false);

        assertEquals(1, scorerDefault.observeSynonymous("0/0", "1/1", null, null));
        assertEquals(1, scorerDefault.observeSynonymous("1/1", "0/0", null, null));
        assertEquals(0, scorerDefault.observeSynonymous("0/1", "1/0", null, null));
        assertEquals(1, scorerDefault.observeSynonymous("0/0", "0/1", null, null));
        assertEquals(0, scorerDefault.observeSynonymous("0/1", "0/1", null, null));
        assertEquals(0, scorerDefault.observeSynonymous("1/1", "1/1", null, null));
        assertEquals(1, scorerDefault.observeSynonymous("1/1/3", "1/2/3", null, null));


        scorerDefault.setOnlyHomozygoteAlleles(true);

        assertEquals(1, scorerDefault.observeSynonymous("0/0", "1/1", null, null));
        assertEquals(1, scorerDefault.observeSynonymous("1/1", "0/0", null, null));
        assertEquals(0, scorerDefault.observeSynonymous("0/0", "0/1", null, null));
        assertEquals(0, scorerDefault.observeSynonymous("0/1", "1/0", null, null));
        assertEquals(0, scorerDefault.observeSynonymous("0/1", "0/1", null, null));
        assertEquals(0, scorerDefault.observeSynonymous("1/1", "1/1", null, null));
        assertEquals(0, scorerDefault.observeSynonymous("1/1/3", "1/2/3", null, null));

    }
}
