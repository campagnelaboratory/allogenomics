package org.campagnelab.allogenomics;

import edu.cornell.med.icb.goby.algorithmic.data.Segment;
import edu.cornell.med.icb.goby.reads.RandomAccessSequenceCache;
import it.unimi.dsi.fastutil.objects.ObjectOpenHashBigSet;
import it.unimi.dsi.fastutil.objects.ObjectSet;
import it.unimi.dsi.lang.MutableString;
import org.campagnelab.allogenomics.annotations.CurrentFrame;
import org.campagnelab.allogenomics.annotations.GobyAnnotations;
import org.campagnelab.allogenomics.annotations.GtfAnnotationParser;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertFalse;

/**
 * @author Fabien Campagne
 *         Date: 2/19/13
 *         Time: 1:15 PM
 */
public class TestTranslation {


    public void translateAnnotations() throws Exception {
        String testDir = "test-data/test-set-2/";
        RandomAccessSequenceCache genome = new RandomAccessSequenceCacheAdapter();
        genome.load(testDir + "Homo_sapiens.GRCh37.70.dna.chromosome.19");
        GobyAnnotations annotations = new GobyAnnotations(new GtfAnnotationParser(),testDir + "/annotations.gtf", (GenomeWithRegistration) genome);
        //19	protein_coding	CDS	291285	291336	.	-	0	 gene_id "ENSG00000141934"; transcript_id "ENST00000269812"; exon_number "1"; gene_name "PPAP2C"; gene_biotype "protein_coding"; transcript_name "PPAP2C-001"; protein_id "ENSP00000269812";
        String expectedProteinSequence = "MQRRWVFVLLDVLCLLVASLPFAILTLVNAPYKRGFYCGDDSIRYPYRPDTITHGLMAGVTITATVILVS" +
                "AGEAYLVYTDRLYSRSDFNNYVAAVYKVLGTFLFGAAVSQSLTDLAKYMIGRLRPNFLAVCDPDWSRVNC" +
                "SVYVQLEKVCRGNPADVTEARLSFYSGHSSFGMYCMVFLALYVQARLCWKWARLLRPTVQFFLVAFALYV" +
                "GYTRVSDYKHHWSDVLVGLLQGALVAALTVCYISDFFKARPPQHCLKEEELERKPSLSLTLTLGEADHNH" +
                "YGYPHSSS";
        ObjectSet<CurrentFrame> currentFrames = new ObjectOpenHashBigSet<CurrentFrame>();
        annotations.getOverlappingTranscriptIds(currentFrames, "19", 291285 + 1, 291285 + 2);
        assertFalse("result cannot be empty", currentFrames.isEmpty());
     /*   int refIndex = genome.getReferenceIndex("19");
        MutableString codonNucleotides = new MutableString();
        MutableString proteinSeq = new MutableString();

        for (CurrentFrame cf : currentFrames) {
            Annotation a = annotations.getAnnotationForTranscript(cf.transcriptId);
            boolean negativeStrand = "-".equalsIgnoreCase(a.getStrand());


            int numSegments = a.getSegments().size();
            if (!negativeStrand) {
                for (int segIndex = 0; segIndex < numSegments; segIndex++) {
                    processSegment(segIndex, genome, refIndex, negativeStrand, a.getSegments().get(segIndex), codonNucleotides, proteinSeq);
                }
            } else {
                for (int segIndex = numSegments - 1; segIndex >= 0; segIndex--) {

                    processSegment(segIndex, genome, refIndex, negativeStrand, a.getSegments().get(segIndex), codonNucleotides, proteinSeq);


                }
            }


        }
        assertEquals(expectedProteinSequence, proteinSeq);
*/
    }


    private void processSegment(int segmentIndex, RandomAccessSequenceCache genome, int refIndex, boolean negativeStrand, Segment segment, MutableString codonNucleotides, MutableString proteinSeq) {
        int frame = Integer.parseInt(segment.getId());
        int start = segment.getStart() - frame-1;
        int end = segment.getEnd() - frame;

        System.out.printf("segmentIndex=%d start=%d end=%d frame=%d %n",segmentIndex, start,end,frame);
        MutableString segmentNucleotides = new MutableString();
        genome.getRange(refIndex, start, end - start, segmentNucleotides);
        if (negativeStrand) {
            reverseComplement(segmentNucleotides);
        }
        codonNucleotides.append(segmentNucleotides);
        codonNucleotides.append("|");
        System.out.println(segmentNucleotides);
        for (int pos = 0; pos < (end - start)/3*3; pos += 3) {
            System.out.println("pos: "+pos);
            ProteinCodonTranslator.appendTranslate(segmentNucleotides.substring(pos, pos + 3), proteinSeq);
        }
    }

    private char complementBase(char base) {
        switch (base) {
            case 'A':
                return 'T';

            case 'C':
                return 'G';

            case 'T':
                return 'A';

            case 'G':
                return 'C';

            default:
                return 'N';


        }
    }

    private void reverseComplement(MutableString codonNucleotides) {
        System.out.println("input: "+codonNucleotides);
        int destIndex = codonNucleotides.length() - 1;
        for (int sourceIndex = 0; sourceIndex < codonNucleotides.length(); sourceIndex++, destIndex--) {
            char sourceBase = codonNucleotides.charAt(sourceIndex);
            char destBase = codonNucleotides.charAt(destIndex);

            codonNucleotides.charAt(destIndex, complementBase(sourceBase));
            codonNucleotides.charAt(sourceIndex, complementBase(destBase));

            if (sourceIndex >= destBase) break;
        }
        System.out.println("reverseComplement: "+codonNucleotides);
    }
}