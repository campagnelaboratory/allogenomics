package org.campagnelab.allogenomics.annotations;

import edu.cornell.med.icb.goby.algorithmic.data.Annotation;
import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import it.unimi.dsi.lang.MutableString;
import org.campagnelab.allogenomics.NamedSequencesGenome;
import org.campagnelab.allogenomics.TranscriptSelection;
import org.junit.Test;

import java.io.StringReader;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertTrue;

/**
 * @author Fabien Campagne
 *         Date: 6/21/14
 *         Time: 2:31 PM
 */
public class TestIlluminaAnnotationParser {
    @Test
    public void testTranscriptCounts() throws Exception {

        NamedSequencesGenome fakeGenome = new NamedSequencesGenome();
        fakeGenome.registerChromosome(new MutableString("11"));


        String content = "Name\tChr\tCoordinate\tGeneSymbol\tGeneLocation\tExonLocation\tCodingStatus\tAminoAcid1|AminoAcid2\n" +
                "cnvi0104169\t11\t110491241\tARHGAP20\tINTRON\tNA\tNA\tNA\n" +
                "cnvi0049425\t11\t1581807\tDUSP8\tINTRON\tNA\tNA\tNA\n" +
                "cnvi0075038\t11\t86286503\tME3\tINTRON\tNA\tNA\tNA\n" +
                "cnvi0057959\t11\t121056607\tTECTA\tINTRON\tNA\tNA\tNA\n" +
                "cnvi0105096\t11\t64409499\tNRXN2\tINTRON\tNA\tNA\tNA\n" +
                "cnvi0079563\t11\t134037133\tNCAPD3\tINTRON\tNA\tNA\tNA\n" +
                "cnvi0104300\t11\t7818081\tOR5P2\tCODING\t188/2\tNA\tNA\n" +
                "cnvi0060555\t11\t1079347\tMUC2\tINTRON\tNA\tNA\tNA\n" +
                "cnvi0101138\t11\t72350829\tPDE2A\tINTRON\tNA\tNA\tNA\n" +
                "cnvi0104177\t11\t110492276\tARHGAP20\tINTRON\tNA\tNA\tNA\n" +
                "cnvi0102077\t11\t1439057\tBRSK2\tINTRON\tNA\tNA\tNA\n" +
                "cnvi0072988\t11\t73307987\tFAM168A\tINTRON\tNA\tNA\tNA\n" +
                "cnvi0056710\t11\t131941031\tNTM\tINTRON\tNA\tNA\tNA";

        IlluminaAnnotationParser parser = new IlluminaAnnotationParser();
        TranscriptSelection selection = new TranscriptSelection(null);

        ObjectArrayList<Annotation> result = parser.parse(new StringReader(content), selection, fakeGenome);
        assertTrue(!result.isEmpty());
        assertEquals(1, result.size());
       assertTrue( result.get(0).getId().equals("OR5P2"));
       assertTrue( result.get(0).getChromosome().equals("11"));
       assertEquals(7818081, result.get(0).getStart());
    }
}
