package org.campagnelab.allogenomics.annotations;

import edu.cornell.med.icb.goby.algorithmic.data.Annotation;
import edu.cornell.med.icb.goby.algorithmic.data.Segment;
import it.unimi.dsi.lang.MutableString;
import org.campagnelab.allogenomics.NamedSequencesGenome;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

/**
 * @author Fabien Campagne
 *         Date: 4/12/13
 *         Time: 6:50 PM
 */
public class GobyGtfAnnotationsTest {
    @Test
    public void testAnnotationComparator() {

        NamedSequencesGenome fakeGenome = new NamedSequencesGenome();
        fakeGenome.registerChromosome(new MutableString("1"));
        fakeGenome.registerChromosome(new MutableString("6"));
        fakeGenome.registerChromosome(new MutableString("2"));

        Annotation a = new Annotation("G1", "6");
        a.addSegment(new Segment(10, 20, "G1_T1", "+"));    // second
        a.chromosomeIndex=fakeGenome.getReferenceIndex("6");
        Annotation b = new Annotation("after G1", "6");     // third
        b.addSegment(new Segment(30, 40, "G2_T1", "+"));
        b.chromosomeIndex=fakeGenome.getReferenceIndex("6");
        Annotation c = new Annotation("before G1", "6");    // first
        c.addSegment(new Segment(1, 2, "G3_T1", "+"));
        c.chromosomeIndex=fakeGenome.getReferenceIndex("6");

        AnnotationComparator comparator = new AnnotationComparator(fakeGenome);
        assertTrue(comparator.compare(a, b) < 0);
        assertTrue(comparator.compare(b, a) > 0);

        assertTrue(comparator.compare(c, a) < 0);
        assertTrue(comparator.compare(c, b) < 0);

        Annotation d = new Annotation("same as a", "6");
        d.chromosomeIndex=fakeGenome.getReferenceIndex("6");
        d.addSegment(new Segment(10, 20, "G1_T1", "+"));    // same position as a
        assertTrue(comparator.compare(a, d) == 0);
        assertTrue(comparator.compare(d, a) == 0);

        Annotation e = new Annotation("same pos as a, on chr 1", "1");
        e.addSegment(new Segment(10, 20, "G1_T1", "+"));    // same position as a
        e.chromosomeIndex=fakeGenome.getReferenceIndex("1");
        assertTrue(comparator.compare(e, a) < 0);     // e occurs before a because chr 1 has index =0 < 1 (chr 6)

        Annotation f = new Annotation("same pos as a, on chr 2", "2");
        f.addSegment(new Segment(10, 20, "G1_T1", "+"));    // same position as a
        f.chromosomeIndex=fakeGenome.getReferenceIndex("2");
        assertTrue(comparator.compare(f, a) > 0);     // f occurs after a because chr 1 has index 2 > 1 (chr 6)
    }
}
