package org.campagnelab.allogenomics;

import com.sun.media.sound.RIFFWriter;
import it.unimi.dsi.fastutil.io.FastBufferedOutputStream;
import org.apache.commons.io.output.WriterOutputStream;
import org.campagnelab.allogenomics.scoring.AlloGenomicsScores;
import org.junit.Test;

import java.io.*;

import static junit.framework.Assert.assertEquals;

/**
 * @author Fabien Campagne
 *         Date: 2/18/13
 *         Time: 3:58 PM
 */
public class TestScoring1 {
    @Test
    public void testTranscriptCounts() throws Exception {
       new File("test-data/test-set-1/file.tsv.gz").delete();

        VcfScorer scorer = new VcfScorer();
        StringWriter stringWriter = new StringWriter();
        PrintStream buffer = new PrintStream(new WriterOutputStream(stringWriter));
        scorer.setOutput(buffer);
        String testDir = "test-data/test-set-1";
        scorer.setTranscriptSelectionFilename(null);
        scorer.setMaxTheoreticalPairs(10000);
        scorer.setTypeOfScore(AlloGenomicsScores.TypeOfScore.PER_TRANSCRIPT);
        scorer.setUseEnsemblTerms(true);
        scorer.process(testDir + "/file.vcf",
                testDir + "/relations.tsv",
                testDir + "/annotations.gtf");
        assertEquals("pair\ttranscriptId\tallo-score\tsynonymous-mismatches\tphenotype\n" +
                "clinical donor:S1|recipient:S2 >> ACR\tENST00000564532\t1\t0\tACR\n", stringWriter.getBuffer().toString());

    }

    @Test
    public void testAnnotationCounts() throws Exception {
        new File("test-data/test-set-3/file.tsv.gz").delete();

        VcfScorer scorer = new VcfScorer();
        StringWriter stringWriter = new StringWriter();
        PrintStream buffer = new PrintStream(new WriterOutputStream(stringWriter));
        scorer.setOutput(buffer);
        String testDir = "test-data/test-set-3";
        scorer.setTranscriptSelectionFilename(null);
        scorer.setMaxTheoreticalPairs(10000);
        scorer.setTypeOfScore(AlloGenomicsScores.TypeOfScore.PER_TRANSCRIPT);
        scorer.setOutputFormat(VcfScorer.OutputFormat.PER_ANNOTATION);
        scorer.process(testDir + "/file.vcf",
                testDir + "/relations.tsv",
                testDir + "/annotations.gtf");
        assertEquals("transcriptId\tscore(ACR[allo])\tscore(ACR[synonymous])\n" +
                "ENST00000564532\t1\t0\n", stringWriter.getBuffer().toString());

    }

    @Test
    public void testTranscriptCountsBDvalFormat() throws Exception {
        new File("test-data/test-set-1/file.tsv.gz").delete();

        VcfScorer scorer = new VcfScorer();
        StringWriter stringWriter = new StringWriter();
        PrintStream buffer = new PrintStream(new WriterOutputStream(stringWriter));
        scorer.setOutput(buffer);
        String testDir = "test-data/test-set-1";
        scorer.setTranscriptSelectionFilename(null);
        scorer.setMaxTheoreticalPairs(10000);
        scorer.setTypeOfScore(AlloGenomicsScores.TypeOfScore.PER_TRANSCRIPT);
        scorer.setOutputFormat(VcfScorer.OutputFormat.BDVAL);
        scorer.process(testDir + "/file.vcf",
                testDir + "/relations.tsv",
                testDir + "/annotations.gtf");
        assertEquals("ID_REF\tclinical donor:S1|recipient:S2 >> ACR\n" +
                "ENST00000564532\t1\n", stringWriter.getBuffer().toString());

    }
}
