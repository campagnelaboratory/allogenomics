package org.campagnelab.allogenomics;

import edu.cornell.med.icb.io.TSVReader;
import it.unimi.dsi.fastutil.objects.*;

import java.io.FileReader;
import java.io.IOException;
import java.util.Collection;
import java.util.List;
import java.util.Random;

/**
 * Stores information about pairs of transplants.
 *
 * @author Fabien Campagne
 *         Date: 2/15/13
 *         Time: 6:16 PM
 */
public class TransplantPairs {
    private Object2ObjectMap<String, DnaSample> samples = new Object2ObjectAVLTreeMap<String, DnaSample>();
    private ObjectSet<TransplantPair> pairs = new ObjectOpenHashSet<TransplantPair>();

    /**
     * Load sample matching and phenotypic information. The TSV file expected by this method must have the following
     * format:
     * <table border=0 cellpadding=0 cellspacing=0 width=908 style='border-collapse:
     * collapse;table-layout:fixed;width:908pt'>
     * <col width=130 style='mso-width-source:userset;mso-width-alt:4754;width:130pt'>
     * <col width=96 style='mso-width-source:userset;mso-width-alt:3510;width:96pt'>
     * <col width=152 style='mso-width-source:userset;mso-width-alt:5558;width:152pt'>
     * <col width=152 style='mso-width-source:userset;mso-width-alt:5558;width:152pt'>
     * <col width=153 style='mso-width-source:userset;mso-width-alt:5595;width:153pt'>
     * <col width=75 span=3 style='width:75pt'>
     * <tr height=13 style='height:13.0pt'>
     * <td height=13 width=200 style='height:13.0pt;width:130pt'>#short sample
     * identifie<span style='display:none'>r</span></td>
     * <td width=96 style='width:96pt'>typeOfSample</td>
     * <td width=152 style='width:152pt'>identifier of matched sam<span
     * style='display:none'>ple</span></td>
     * <td width=200 style='width:152pt'>Transplant phenotype (at<span
     * style='display:none'><span style="mso-spacerun:yes">&nbsp;</span>one year
     * post-transplant)</span></td>
     * <p/>
     * </tr>
     * <tr height=13 style='height:13.0pt'>
     * <td height=13 style='height:13.0pt'>S1</td>
     * <td>Donor</td>
     * <td>S2</td>
     * <td>ACR</td>
     * <p/>
     * </tr>
     * <tr height=13 style='height:13.0pt'>
     * <td height=13 style='height:13.0pt'>S2</td>
     * <td>Recipient</td>
     * <td>S1</td>
     * <td>ACR</td>
     * <p/>
     * </tr>
     * <tr height=13 style='height:13.0pt'>
     * <td height=13 style='height:13.0pt'>S3</td>
     * <td>Recipient</td>
     * <td>S4</td>
     * <td>Normal</td>
     * <p/>
     * </tr>
     * <tr height=13 style='height:13.0pt'>
     * <td height=13 style='height:13.0pt'>S4</td>
     * <td>Donor</td>
     * <td>S3</td>
     * <td>Normal</td>
     * </tr>
     * </table>
     *
     * @param filename
     * @throws IOException
     */
    public void loadPairs(String filename) throws IOException {
        TSVReader tsvReader = new TSVReader(new FileReader(filename));
        int lineNumber = 1;
        tsvReader.setCommentPrefix("#");
        while (tsvReader.hasNext()) {
            if (tsvReader.isCommentLine()) {
                tsvReader.skip();
            } else {
                if (lineNumber > 1) {
                    String sampleId = tsvReader.getString();
                    String kindOfDNA = tsvReader.getString();
                    String sampleIdForPair = tsvReader.getString();
                    String phenotype = tsvReader.numTokens() >= 4 ? tsvReader.getString() : null;
                    DnaSample lookupSample = lookup(sampleId, kindOfDNA);
                    DnaSample pairedSample = lookup(sampleIdForPair, null);
                    if (lookupSample != null &&
                            pairedSample != null) {
                        TransplantPair pair = new TransplantPair(lookupSample, pairedSample);
                        pair.setClinical(true);
                        if (phenotype != null) {

                            pair.setPhenotype(phenotype);
                        }
                        pairs.add(pair);
                        System.out.println("Reading pair: "+pair);
                    }
                }
                tsvReader.next();
                lineNumber++;
            }

        }
        System.out.printf("Loaded %d clinical pairs and %d samples%n", pairs.size(), samples.size());
    }

    private DnaSample lookup(String sampleId, String kindOfDNA) {

        if (!samples.containsKey(sampleId) && kindOfDNA != null) {

            DnaSample sample = new DnaSample();
            sample.kind = kindOfDNA;
            sample.sampleId = sampleId;
            samples.put(sampleId, sample);
            return sample;
        } else {
            return samples.get(sampleId);
        }
    }

    public Collection<String> getAllSampleIds() {
        Collection<String> result = new ObjectArrayList<String>();
        for (DnaSample sample : samples.values()) {

            result.add(sample.sampleId);
        }
        return result;
    }

    public Collection<TransplantPair> getClinicalPairs() {
        return pairs;
    }

    /**
     * Construct all the theoretically possible pairs of DNA samples. This will create pairs that were not actually
     * transplanted by simple combinatorial.
     *
     * @return list of theoretical pairs.
     * @param maxTheoreticalPairs  The maximum number of pairs to generate.
     */
    public List<TransplantPair> getAllTheoreticalPairs(int maxTheoreticalPairs) {
        List<TransplantPair> result = new ObjectArrayList<TransplantPair>();
        Random random = new Random(8239829383L);
        int sqrtMaxPair= (int) Math.round(Math.sqrt(maxTheoreticalPairs));
        List<DnaSample> donorSamples = new ObjectArrayList<DnaSample>();
        List<DnaSample> recipientSamples = new ObjectArrayList<DnaSample>();
        // construct a random subset of at most 10,000 theoretical pairs. This will keep the computation
        // tractable in cases where the number of samples increase a lot.
        donorSamples = randomSampleKnuth(random, filterPhenotype(samples.values(), "donor"), sqrtMaxPair);
        recipientSamples = randomSampleKnuth(random, filterPhenotype(samples.values(), "recipient"), sqrtMaxPair);
        for (DnaSample sample1 : donorSamples) {

            for (DnaSample sample2 : recipientSamples) {

                TransplantPair theoreticalPair = new TransplantPair(sample1, sample2);
                if (pairs.contains(theoreticalPair)) {
                    theoreticalPair.setClinical(true);
                    associatePhenotype(pairs, theoreticalPair);
                }
                result.add(theoreticalPair);
            }
        }
        return result;
    }

    private void associatePhenotype(ObjectSet<TransplantPair> pairs, TransplantPair theoreticalPair) {
        for (TransplantPair pair : pairs) {
            if (pair.equals(theoreticalPair)) {
                theoreticalPair.setPhenotype(pair.getPhenotype());
                return;
            }
        }
    }

    /**
     * Return only the subset of DNASamples matching the given phenotype.
     *
     * @param values DNASamples to filter.
     * @param donor  phenotype the result will contain.
     * @return
     */
    private List<DnaSample> filterPhenotype(ObjectCollection<DnaSample> values, String donor) {
        List<DnaSample> result = new ObjectArrayList<DnaSample>();
        for (DnaSample value : values) {
            if (donor.equalsIgnoreCase(value.kind)) {
                result.add(value);
            }
        }
        return result;
    }

    /**
     * Choose m elements at random from list items.
     *
     * @param items the list of items to choose from.
     * @param m     the number of random element to return
     * @param <T>
     * @return
     */
    public <T> List<T> randomSampleKnuth(Random random, List<T> items, int m) {
        if (items.size() < m) {
            // no need to sub-sample, small enough:
            return items;
        }

        for (int i = 0; i < m; i++) {

            int pos = i + random.nextInt(items.size() - i);

            T tmp = items.get(pos);

            items.set(pos, items.get(i));

            items.set(i, tmp);

        }

        return items.subList(0, m);

    }

}
