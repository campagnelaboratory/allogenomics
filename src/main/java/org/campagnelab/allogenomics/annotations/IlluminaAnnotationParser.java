package org.campagnelab.allogenomics.annotations;

import edu.cornell.med.icb.goby.algorithmic.data.Annotation;
import edu.cornell.med.icb.goby.algorithmic.data.Segment;
import it.unimi.dsi.fastutil.objects.Object2ObjectAVLTreeMap;
import it.unimi.dsi.fastutil.objects.Object2ObjectMap;
import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import it.unimi.dsi.io.FastBufferedReader;
import it.unimi.dsi.io.LineIterator;
import it.unimi.dsi.lang.MutableString;
import org.campagnelab.allogenomics.GenomeWithRegistration;
import org.campagnelab.allogenomics.TranscriptSelection;

import java.io.FileNotFoundException;
import java.io.Reader;

/**
 * Annotation parser implementation that can parse annotations for the Illumina beads array platform(s).
 *
 * @author Fabien Campagne
 *         Date: 6/21/14
 *         Time: 12:05 PM
 */
public class IlluminaAnnotationParser implements AnnotationParser {
    public IlluminaAnnotationParser() {
    }

    private void extractAttributes(Object2ObjectMap<String, String> attributeMap, String[] line) {
        attributeMap.clear();

        attributeMap.put("transcript_id", line[3].trim());

    }

    public ObjectArrayList<Annotation> parse(Reader annotationReader, TranscriptSelection transcriptSelection, GenomeWithRegistration genome) throws FileNotFoundException {
        LineIterator it = new LineIterator(new FastBufferedReader(annotationReader));

        Object2ObjectMap<String, String> attributes = new Object2ObjectAVLTreeMap<String, String>();
        Object2ObjectAVLTreeMap<String, Annotation> transcriptMap = new Object2ObjectAVLTreeMap<String, Annotation>();
        ObjectArrayList<Annotation> anns = new ObjectArrayList<Annotation>();

        while (it.hasNext()) {
            MutableString line = it.next();
            if (line.startsWith("Name")) continue;
            String[] tokens = line.toString().split("\t");
            String variationContext = tokens[4];
            if (!"CODING".equals(variationContext)) continue;

            extractAttributes(attributes, tokens);
            String chromosome = tokens[1];
            int start = Integer.parseInt(tokens[2]);
            int end = start + 1;
            int frame = +1;

            String transcript = attributes.containsKey("transcript_id") ?
                    attributes.get("transcript_id") :
                    String.format("%s:%d-%d(%d)", chromosome, start, end, frame);


            MutableString chromosomeMut = new MutableString(chromosome);
            if (genome != null) {
                genome.registerChromosome(chromosomeMut);
            }
            int chromosomeIndex = genome.getReferenceIndex(chromosome);
            Annotation ann = transcriptMap.get(transcript);
            if (chromosomeIndex != -1 && transcriptSelection.containsTranscriptId(transcript)) {
                String strand = "+";
                if (ann == null) {
                    ann = new Annotation(transcript, chromosome, strand, chromosomeIndex);
                    ann.chromosomeIndex=chromosomeIndex;
                    anns.add(ann);
                    transcriptMap.put(transcript, ann);
                }

                // store frame in the Segment id, as a String.
                ann.addSegment(new Segment(start, end, Integer.toString(frame), strand));
            }

        }
        return anns;
    }
}
