package org.campagnelab.allogenomics.annotations;

import it.unimi.dsi.fastutil.objects.ObjectSet;

/**
 * @author Fabien Campagne
 *         Date: 2/17/13
 *         Time: 11:04 AM
 */
public interface GtfAnnotationsI {

    void getOverlappingTranscriptIds(ObjectSet<CurrentFrame> transcriptSet,
                                     String sequence,
                                     int start, int end)             throws Exception;


    boolean isEmpty();
}
