package org.campagnelab.allogenomics.annotations;

import com.google.common.collect.ComparisonChain;
import edu.cornell.med.icb.goby.algorithmic.data.Annotation;
import edu.cornell.med.icb.goby.reads.RandomAccessSequenceInterface;

import java.util.Comparator;

/**
 * Compare Goby annotations by chromosome index, then positions.
 *
 * @author Fabien Campagne
 *         Date: 4/12/13
 *         Time: 6:54 PM
 */
public class AnnotationComparator implements Comparator<Annotation> {
    private final RandomAccessSequenceInterface genome;

    public AnnotationComparator(RandomAccessSequenceInterface genome) {
        this.genome = genome;
    }


    /**
     * Compare two annotations.
     *
     * @param a
     * @param b
     * @return Is negative when a appears before b in the genome, zero when a occurs at the same position as b, or positive otherwise.
     */
    public int compare(Annotation a, Annotation b) {
        return ComparisonChain.start()
                .compare(a.chromosomeIndex, b.chromosomeIndex)
                .compare(a.getStart(), b.getStart())
               .result();


    }

};


