package org.campagnelab.allogenomics.annotations;

import edu.cornell.med.icb.goby.algorithmic.algorithm.SortedAnnotations;
import edu.cornell.med.icb.goby.algorithmic.data.Annotation;
import edu.cornell.med.icb.goby.algorithmic.data.Segment;
import it.unimi.dsi.fastutil.ints.IntAVLTreeSet;
import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import it.unimi.dsi.fastutil.objects.ObjectSet;

import org.campagnelab.allogenomics.GenomeWithRegistration;
import org.campagnelab.allogenomics.NamedSequencesGenome;
import org.campagnelab.allogenomics.TranscriptSelection;

import java.io.FileReader;
import java.text.NumberFormat;
import java.text.ParsePosition;
import java.util.Collections;
import java.util.Comparator;


/**
 * An implementation of GtfAnnotationsI that delegates to Goby's SortedAnnotation class.
 *
 * @author Fabien Campagne
 *         Date: 2/17/13
 *         Time: 11:02 AM
 */
public class GobyAnnotations implements GtfAnnotationsI {
    private SortedAnnotations sortedAnnotations;
    private GenomeWithRegistration genome = new NamedSequencesGenome();
    private final Annotation[] annotations;
    protected Comparator<? super Annotation> annotationComparator;

    public GobyAnnotations(AnnotationParser parser, String gtfAnnotationsFilename, GenomeWithRegistration genomeArg) throws Exception {
        this(parser, gtfAnnotationsFilename, genomeArg, new TranscriptSelection(null));
    }

    public GobyAnnotations(AnnotationParser parser,
                           String gtfAnnotationsFilename,
                           GenomeWithRegistration genomeArg,
                           TranscriptSelection transcriptSelection) throws Exception {

        GenomeWithRegistration fakeGenome = null;
        if (genomeArg != null) {

            genome = genomeArg;

        } else {
            fakeGenome = new NamedSequencesGenome();
            genome = fakeGenome;
        }

        System.out.println("Loading annotations");
        ObjectArrayList<Annotation> anns = parser.parse(new FileReader(gtfAnnotationsFilename),
                transcriptSelection, genome);
        if (fakeGenome != null) {
            fakeGenome.prepare();
        }
        sortedAnnotations = new SortedAnnotations();
        sortedAnnotations.setGenome(genome);

        for (Annotation a : anns) {
            a.sortSegments();
        }
        annotationComparator = new AnnotationComparator(genome);
        Collections.sort(anns, annotationComparator);

        annotations = anns.toArray(new Annotation[anns.size()]);
        sortedAnnotations.setAnnotations(annotations);
    }

    private static NumberFormat formatter = NumberFormat.getInstance();

    public static boolean isNumeric(String str) {

        ParsePosition pos = new ParsePosition(0);
        formatter.parse(str, pos);
        return str.length() == pos.getIndex();
    }


    String lastChromosome = "";
    int currentReferenceIndex;


    public void getOverlappingTranscriptIds(ObjectSet<CurrentFrame> transcriptSet, String sequence, int start, int end) throws Exception {
        transcriptSet.clear();
        if (!sequence.equals(lastChromosome)) {
            lastChromosome = sequence;

            currentReferenceIndex = genome.getReferenceIndex(sequence);
            if (currentReferenceIndex >= 0) {
                //    System.out.printf("Annotations have advanced to chromosome: %s%n",sequence);
                sortedAnnotations.advanceToChromosome(currentReferenceIndex);
            } else {
                System.err.println("Genome does not contain this chromosome: " + sequence);
                // genome does not contain this chromosome.
                return;
            }

        }
        if (currentReferenceIndex >= 0) {
            sortedAnnotations.advanceToPosition(currentReferenceIndex, start);
            IntAVLTreeSet indices = sortedAnnotations.getValidAnnotationIndices();
            if (indices.size() > 0) {
                // System.out.printf("chr=%s start=%d transcripts=%s%n", sequence, start, indices.toString());
                for (int index : indices) {
                    //     System.out.println(annotations[index]);
                    Annotation annotation = annotations[index];
                    assert annotation.overlap(sequence, start) : "transcripts must overlap position";
                    String transcriptId = annotation.getId();
                    Segment overlappingSegment = null;

                    for (Segment seg : annotation.getSegments()) {
                        if (seg.overlapPosition(start)) {
                            overlappingSegment = seg;
                        }
                    }
                    assert overlappingSegment != null : "one segment must overlap.";
                    // frame is stored as a string in the segment id:
                    int frame = Integer.parseInt(overlappingSegment.getId());

                    transcriptSet.add(new CurrentFrame(transcriptId, frame, sequence, start));
                }
            }
        }
        // System.out.println(indices);

    }

    public boolean isEmpty() {
        return annotations.length == 0;
    }

    public Annotation getAnnotationForTranscript(String transcriptId) {
        for (Annotation ann : annotations) {
            if (ann.getId().equalsIgnoreCase(transcriptId)) {
                return ann;
            }
        }
        return null;
    }

}
