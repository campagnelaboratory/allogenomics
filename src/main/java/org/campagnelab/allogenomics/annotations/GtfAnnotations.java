package org.campagnelab.allogenomics.annotations;

import it.unimi.dsi.fastutil.objects.ObjectSet;
import it.unimi.dsi.lang.MutableString;
import org.biojava3.core.sequence.DNASequence;
import org.biojava3.core.sequence.ProteinSequence;
import org.biojava3.core.sequence.RNASequence;
import org.biojava3.core.sequence.transcription.TranscriptionEngine;
import org.biojava3.genome.parsers.gff.*;

import java.io.IOException;

/**
 * Support to load GTF annotations and retrieve features overlapping a genomic region.
 *
 * @author Fabien Campagne
 *         Date: 2/16/13
 *         Time: 3:30 PM
 */
public class GtfAnnotations {
    FeatureList featureStore;

    public GtfAnnotations(String gtfAnnotationsFilename) throws Exception {
        GFF3Reader reader = new GFF3Reader();

        FeatureList features = GFF3Reader.read(gtfAnnotationsFilename);
        featureStore = features.selectByType("CDS");
        /*
        for (FeatureI feature : featureStore.selectOverlapping("X", new Location(205400, 206400), true)) {
            if (feature instanceof Feature) {
                Feature f = ((Feature) feature);
                if ("CDS".equals(feature.type()) && "protein_coding".equals(f.source())) {
                    System.out.println(feature);
                }
            }
        } */
        /*
        TranscriptionEngine.Builder b = new TranscriptionEngine.Builder();
        b.table(11).initMet(true).trimStop(true);
        TranscriptionEngine engine = b.build();
        DNASequence dna = new DNASequence("ATG");
        RNASequence rna = dna.getRNASequence(engine);
        ProteinSequence protein = rna.getProteinSequence(engine);

          */

    }

    /**
     * Query the GTF store for features overlapping with a region.
     *
     * @param sequence
     * @param start
     * @param end
     * @param bothStrands
     * @return
     * @throws Exception
     */
    public FeatureList query(String sequence, int start, int end, boolean bothStrands) throws Exception {
        return featureStore.selectOverlapping(sequence, new Location(start, end), bothStrands);
    }

    public void getOverlappingTranscriptIds(ObjectSet<String> transcriptSet, String sequence, int start, int end) throws Exception {
        transcriptSet.clear();
        for (FeatureI feature : query(sequence, start, end, true)) {
          transcriptSet.add( feature.getAttribute("transcript_id")) ;
        }

    }
}