package org.campagnelab.allogenomics.annotations;

/**
 * Information about a valid frame of translation, which overlaps this position.
 *
 * @author Fabien Campagne
 *         Date: 2/19/13
 *         Time: 12:20 PM
 */
public class CurrentFrame {
    public String transcriptId;

    public String chromosome;
    public int position;
    /**
     * 0, 1 or 2, indicates how much to substract to position to find the place where the codon would start.
     */
    public int frame;

    public CurrentFrame(String transcript_id, int frame, String chromosome, int position) {
        this.transcriptId=transcript_id;
        this.frame=frame;
        this.chromosome=chromosome;
        this.position=position;

    }

}
