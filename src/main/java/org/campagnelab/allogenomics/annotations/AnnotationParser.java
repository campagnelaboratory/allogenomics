package org.campagnelab.allogenomics.annotations;

import edu.cornell.med.icb.goby.algorithmic.data.Annotation;
import edu.cornell.med.icb.goby.reads.RandomAccessSequenceInterface;
import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import org.campagnelab.allogenomics.GenomeWithRegistration;
import org.campagnelab.allogenomics.NamedSequencesGenome;
import org.campagnelab.allogenomics.TranscriptSelection;

import java.io.FileNotFoundException;
import java.io.Reader;

/**
 * Interface for annotation parsers.
 * @author Fabien Campagne
 *         Date: 6/21/14
 *         Time: 12:08 PM
 */
public interface AnnotationParser {
    public ObjectArrayList<Annotation> parse(Reader annotationReader,
                                             TranscriptSelection transcriptSelection,
                                             GenomeWithRegistration genome) throws FileNotFoundException;
}
