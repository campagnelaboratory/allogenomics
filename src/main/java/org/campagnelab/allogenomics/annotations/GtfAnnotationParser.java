package org.campagnelab.allogenomics.annotations;

import edu.cornell.med.icb.goby.algorithmic.data.Annotation;
import edu.cornell.med.icb.goby.algorithmic.data.Segment;
import it.unimi.dsi.fastutil.objects.Object2ObjectAVLTreeMap;
import it.unimi.dsi.fastutil.objects.Object2ObjectMap;
import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import it.unimi.dsi.io.FastBufferedReader;
import it.unimi.dsi.io.LineIterator;
import it.unimi.dsi.lang.MutableString;
import org.campagnelab.allogenomics.GenomeWithRegistration;
import org.campagnelab.allogenomics.TranscriptSelection;

import java.io.FileNotFoundException;
import java.io.Reader;

/**
 * Annotation parser implementation that can parse annotations in the GTF3 format.
 * @author Fabien Campagne
 *         Date: 6/21/14
 *         Time: 12:05 PM
 */
public class GtfAnnotationParser implements AnnotationParser {
    public GtfAnnotationParser() {
    }

    private void extractAttributes(Object2ObjectMap<String, String> attributeMap, String attributeToken) {
        attributeMap.clear();
        String[] attributeValuePairs = attributeToken.split(";");
        for (String avp : attributeValuePairs) {
            String[] key_value = avp.replaceAll("\"", " ").split(" ");
            if (key_value.length < 4) {
                continue;
            }
            attributeMap.put(key_value[1].trim(), key_value[3].trim());
        /*        String keyTrimed = key_value[1].trim();
                if (key_value[2].length() > 0) {
                    attributeMap.put(keyTrimed, key_value[2].trim());
                } else if (key_value[3].length() > 0) {
                    attributeMap.put(keyTrimed, key_value[3].trim());
                }
                 */
        }
    }

    public ObjectArrayList<Annotation> parse(Reader annotationReader, TranscriptSelection transcriptSelection, GenomeWithRegistration genome) throws FileNotFoundException {
        LineIterator it = new LineIterator(new FastBufferedReader(annotationReader));

        Object2ObjectMap<String, String> attributes = new Object2ObjectAVLTreeMap<String, String>();
        Object2ObjectAVLTreeMap<String, Annotation> transcriptMap = new Object2ObjectAVLTreeMap<String, Annotation>();
        ObjectArrayList<Annotation> anns = new ObjectArrayList<Annotation>();

        while (it.hasNext()) {
            MutableString line = it.next();
            String[] tokens = line.toString().split("\t");
            if (!("protein_coding".equalsIgnoreCase(tokens[1]) && "CDS".equalsIgnoreCase(tokens[2]))) continue;
            extractAttributes(attributes, tokens[8]);
            String chromosome = tokens[0];
            if (GobyAnnotations.isNumeric(tokens[3])) {
                int start = Integer.parseInt(tokens[3]);
                int end = Integer.parseInt(tokens[4]);
                int frame = Integer.parseInt(tokens[7]);

                String transcript = attributes.containsKey("transcript_id") ?
                        attributes.get("transcript_id") :
                        String.format("%s:%d-%d(%d)", chromosome, start, end, frame);


                MutableString chromosomeMut = new MutableString(chromosome);
                if (genome != null) {
                    genome.registerChromosome(chromosomeMut);
                }
                int chromosomeIndex = genome.getReferenceIndex(chromosome);
                Annotation ann = transcriptMap.get(transcript);
                if (chromosomeIndex != -1 && transcriptSelection.containsTranscriptId(transcript)) {
                    String strand = tokens[6];
                    if (ann == null) {
                        ann = new Annotation(transcript, chromosome, strand, chromosomeIndex);
                        anns.add(ann);
                        transcriptMap.put(transcript, ann);
                    }

                    // store frame in the Segment id, as a String.
                    ann.addSegment(new Segment(start, end, Integer.toString(frame), strand));
                }
            }

        }
        return anns;
    }
}
