package org.campagnelab.allogenomics.util;

/**
 * @author Fabien Campagne
 *         Date: 6/21/14
 *         Time: 1:27 PM
 */
class LineInfo {
        String sampleId;
        String patientId;
        String typeInPair;
        String pairId;

    LineInfo(String sampleId, String patientId, String typeInPair, String pairId) {
        this.sampleId = sampleId;
        this.patientId = patientId;
        this.typeInPair = typeInPair;
        this.pairId = pairId;
    }
}