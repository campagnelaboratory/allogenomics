package org.campagnelab.allogenomics.util;

import it.unimi.dsi.fastutil.objects.ObjectArrayList;

/**
 * @author Fabien Campagne
 *         Date: 6/21/14
 *         Time: 1:23 PM
 */
public class GeneratePairs {


    public static void main(String[] args) {
        String ids[] = {"2-P1-Do-1",
                "1-P1-Re-1",
                "3-P2-Re-2",
                "4-P2-Do-2",
                "5-P3-Re-3",
                "6-P3-Do-3",
                "7-P4-Re-4",
                "8-P4-Do-4",
                "9-P5-Re-5",
                "10-P5-Do-5",
                "11-P6-Re-6",
                "12-P6-Do-6",
                "13-P7-Re-7",
                "14-P7-Do-7",
                "15-P8-Re-8",
                "16-P8-Do-8",
                "17-P9-Re-9",
                "18-P9-Do-9",
                "19-P10-Re-10",
                "20-P10-Do-10",
                "21-P11-Re-11",
                "22-P11-Do-11",
                "23-P12-Re-12",
                "24-P12-Do-12",
                "25-P13-Re-13",
                "26-P13-Do-13",
                "27-P14-Re-14",
                "28-P14-Do-14",
                "29-P15-Re-15",
                "30-P15-Do-15",
                "31-P16-Re-16",
                "32-P16-Do-16",
                "33-P17-Re-17",
                "34-P17-Do-17",
                "35-P18-Re-18",
                "36-P18-Do-18",
                "37-P19-Re-19",
                "38-P19-Do-19",
                "39-P20-Re-20",
                "40-P20-Do-20",
                "41-P21-Re-21",
                "42-P21-Do-21",
                "43-P22-Re-22",
                "44-P22-Do-22",
                "45-P23-Re-23",
                "46-P23-Do-23",
                "47-P24-Re-24",
                "48-P24-Do-24"};
        ObjectArrayList<String> pairIds = new ObjectArrayList<String>();
        ObjectArrayList<LineInfo> lines = new ObjectArrayList<LineInfo>();

        for (String id : ids) {

            String[] tokens = id.split("-");
            String sampleId1 = id;
            String patientId = tokens[1];
            String typeInPair = tokens[2];
            String pairId = tokens[3];
            LineInfo i = new LineInfo(sampleId1, patientId, typeInPair, pairId);
            lines.add(i);
            pairIds.add(pairId);

        }
        System.out.println("#short sample identifier        typeOfSample    identifier of matched sample    Transplant Phenotype (given for both donor and recipient genomes, but assessed in recipient at ~one year)");
        final boolean correct = true;
        for (String pairId : pairIds) {
            LineInfo donor = findWithType(lines, "Do", pairId);
            LineInfo recipient = findWithType(lines, "Re", pairId);
            if (correct) {
                System.out.print(String.format("%s\tDonor\t%s\tunknown%n", donor.sampleId, recipient.sampleId));
                System.out.print(String.format("%s\tRecipient\t%s\tunknown%n", recipient.sampleId, donor.sampleId));
            } else {
                // reverse donor and recipient to run a negative control:
                System.out.print(String.format("%s\tDonor\t%s\tunknown%n", recipient.sampleId, donor.sampleId));
                System.out.print(String.format("%s\tRecipient\t%s\tunknown%n", donor.sampleId, recipient.sampleId));

            }
        }
    }

    private static LineInfo findWithType(ObjectArrayList<LineInfo> lines, String typeInPair, String pairId) {
        for (LineInfo line : lines) {
            if (line.pairId.equals(pairId) && typeInPair.equals(line.typeInPair)) return line;
        }
        return null;
    }


}
