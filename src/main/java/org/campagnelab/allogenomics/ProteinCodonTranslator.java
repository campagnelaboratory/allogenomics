package org.campagnelab.allogenomics;

import it.unimi.dsi.fastutil.objects.Object2ObjectAVLTreeMap;
import it.unimi.dsi.fastutil.objects.Object2ObjectMap;
import it.unimi.dsi.lang.MutableString;

import java.io.StringReader;

/**
 * @author Fabien Campagne
 *         Date: 2/19/13
 *         Time: 6:19 PM
 */
public class ProteinCodonTranslator {
    static Object2ObjectMap<MutableString, MutableString> codonTranslations ;

    static {
        String content = "TTT Phe (F)\n" +
                "        TTC Phe (F)\n" +
                "        TTA Leu (L)\n" +
                "        TTG Leu (L)\n" +
                "        TCT Ser (S)\n" +
                "        TCC Ser (S)\n" +
                "        TCA Ser (S)\n" +
                "        TCG Ser (S)\n" +
                "        TAT Tyr (Y)\n" +
                "        TAC NOTUSED (?)\n" +
                "        TAA STOP (*)\n" +
                "        TAG STOP (*)\n" +
                "        TGT Cys (C)\n" +
                "        TGC NOTUSED (?)\n" +
                "        TGA STOP (*)\n" +
                "        TGG Trp (W)\n" +
                "        CTT Leu (L)\n" +
                "        CTC Leu (L)\n" +
                "        CTA Leu (L)\n" +
                "        CTG Leu (L)\n" +
                "        CCT Pro (P)\n" +
                "        CCC Pro (P)\n" +
                "        CCA Pro (P)\n" +
                "        CCG Pro (P)\n" +
                "        CAT His (H)\n" +
                "        CAC His (H)\n" +
                "        CAA Gln (Q)\n" +
                "        CAG Gln (Q)\n" +
                "        CGT Arg (R)\n" +
                "        CGC Arg (R)\n" +
                "        CGA Arg (R)\n" +
                "        CGG Arg (R)\n" +
                "        ATT Ile (I)\n" +
                "        ATC Ile (I)\n" +
                "        ATA Ile (I)\n" +
                "        ATG Met (M)\n" +
                "        ACT Thr (T)\n" +
                "        ACC Thr (T)\n" +
                "        ACA Thr (T)\n" +
                "        ACG Thr (T)\n" +
                "        AAT Asn (N)\n" +
                "        AAC Asn (N)\n" +
                "        AAA Lys (K)\n" +
                "        AAG Lys (K)\n" +
                "        AGT Ser (S)\n" +
                "        AGC Ser (S)\n" +
                "        AGA Arg (R)\n" +
                "        AGG Arg (R)\n" +
                "        GTT Val (V)\n" +
                "        GTC Val (V)\n" +
                "        GTA Val (V)\n" +
                "        GTG Val (V)\n" +
                "        GCT Ala (A)\n" +
                "        GCC Ala (A)\n" +
                "        GCA Ala (A)\n" +
                "        GCG Ala (A)\n" +
                "        GAT Asp (D)\n" +
                "        GAC Asp (D)\n" +
                "        GAA Glu (E)\n" +
                "        GAG Glu (E)\n" +
                "        GGT Gly (G)\n" +
                "        GGC Gly (G)\n" +
                "        GGA Gly (G)\n" +
                "        GGG Gly (G)\n";

        codonTranslations= new Object2ObjectAVLTreeMap<MutableString, MutableString>();
        for (String codonDef : content.split("[\n]")) {
            String tokens[] = codonDef.trim().split("[ ]");
            codonTranslations.put(new MutableString(tokens[0]).compact(), new MutableString(tokens[2].replaceAll("[()]", "")).compact());
        }


    }

    public static void appendTranslate(MutableString codonNucleotides, MutableString protein) {
        protein.append(codonTranslations.get(codonNucleotides));
    }
}
