package org.campagnelab.allogenomics;

import edu.cornell.med.icb.goby.readers.vcf.VCFParser;
import edu.cornell.med.icb.goby.stats.TSVWriter;
import edu.cornell.med.icb.iterators.TsvLineIterator;
import edu.cornell.med.icb.maps.LinkedHashToMultiTypeMap;
import it.unimi.dsi.fastutil.objects.Object2ObjectOpenHashMap;
import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import it.unimi.dsi.fastutil.objects.ObjectArraySet;
import it.unimi.dsi.fastutil.objects.ObjectSet;
import it.unimi.dsi.lang.MutableString;
import it.unimi.dsi.logging.ProgressLogger;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.campagnelab.allogenomics.annotations.*;
import org.campagnelab.allogenomics.scoring.AlloGenomicsScores;

import java.io.*;
import java.util.Collection;
import java.util.Iterator;
import java.util.zip.GZIPOutputStream;

/**
 * Scores pairs described in a VCF file.
 *
 * @author Fabien Campagne
 *         Date: 2/18/13
 *         Time: 9:35 AM
 */
public class VcfScorer {
    private Object2ObjectOpenHashMap<String, String> vcfIdToDnaSampleId = new Object2ObjectOpenHashMap<String, String>();
    private Object2ObjectOpenHashMap<String, String> dnaSampleIdToVcfId = new Object2ObjectOpenHashMap<String, String>();
    private String transcriptSelectionFilename;


    final private MutableString indelMutableString = new MutableString("INDEL");
    private PrintStream output;
    private int maxTheoreticalPairs = 10000;
    /**
     * A true value in the below field indicates that only non-synonymous variants are considered when calculating the
     * score.
     */
    private boolean filterNonSynonymousCoding = false;
    private int processMax = Integer.MAX_VALUE;
    private boolean useEnsemblTerms = true;
    private String genomeBasename;
    private boolean onlyHomozygousAlleles;
    private boolean noDash;

    public void setMeasuredSiteFilename(String measuredSiteFilename) {
        this.measuredSiteFilename = measuredSiteFilename;
    }

    private String measuredSiteFilename;

    public void setUseEnsemblTerms(boolean useEnsemblTerms) {
        this.useEnsemblTerms = useEnsemblTerms;
        if (useEnsemblTerms) this.useVepTerms = false;
    }

    public void setUseVepTerms(boolean useVepTerms) {

        this.useVepTerms = useVepTerms;
        if (useVepTerms) this.useEnsemblTerms = false;
    }

    private boolean useVepTerms;

    public void setDepthThreshold(int depthThreshold) {
        this.depthThreshold = depthThreshold;
    }

    /**
     * Minimum depth to consider a genomic site typed.
     */

    private int depthThreshold = 10;

    public void setTypeOfScore(AlloGenomicsScores.TypeOfScore typeOfScore) {
        this.typeOfScore = typeOfScore;
    }

    private AlloGenomicsScores.TypeOfScore typeOfScore = AlloGenomicsScores.TypeOfScore.GLOBAL;


    public void setConsiderIndels(boolean considerIndels) {
        this.considerIndels = considerIndels;
    }

    /**
     * When this flag is true, indel variations are considered when estimating allo scores.
     */
    private boolean considerIndels = false;
    private boolean useBioJavaGtf = false;
    private boolean clinical;

    public void setMaxDepthPerExome(int maxDepthPerExome) {
        this.maxDepthPerExome = maxDepthPerExome;
    }

    private int maxDepthPerExome = Integer.MAX_VALUE;

    /**
     * Set allSites to true to completely ignore annotations and score each site in the input.
     * @param allSites
     */
    public void setAllSites(boolean allSites) {

        this.allSites = allSites;
    }

    /**
     * If this flag is true, all sites are considered, ignoring annotations completely.
     */
    boolean allSites;

    public VcfScorer() {

    }

    public void process(String vcfFilename, String relationFilename, String annotationFilename) throws Exception {

        int numVcfItems = 0;
        long earlyBreak = processMax;
        TranscriptSelection transcriptSelection = new TranscriptSelection(transcriptSelectionFilename);
        TransplantPairs pairs = new TransplantPairs();
        pairs.loadPairs(relationFilename);
        GenomeWithRegistration genome = null;

        {
            VCFParser parser = new VCFParser(vcfFilename);
            parser.readHeader();
            // Note that allogenomics files require CacheFieldPermutation to be false.
            parser.setCacheFieldPermutation(false);

            String tsvFilename = convertVcfToTsv(parser, vcfFilename, numVcfItems);
            parser.close();

        }
        String genomeBasename = getGenomeBasename(vcfFilename);
        File genomeFile = new File(genomeBasename);
        if ((genomeFile.canRead()) && genomeFile.length() > 0) {

            genome = NamedSequencesGenome.load(getGenomeBasename(vcfFilename));

        } else {

            System.out.println("Scanning TSV to observe chromosome order..");
            ProgressLogger pg = new ProgressLogger();
            pg.displayFreeMemory = true;
            pg.start("Scanning TSV to observe chromosome order");
            pg.itemsName = "sites";
            // read the sample names from TSV first line:
            String tsvOutputFilename = getTsvFilename(vcfFilename);
            TsvLineIterator tsvLineReader = new TsvLineIterator(tsvOutputFilename);
            Iterator<LinkedHashToMultiTypeMap<String>> iterator = tsvLineReader.iterator();
            NamedSequencesGenome vcfGenome = new NamedSequencesGenome();

            int count = 0;

            while (iterator.hasNext()) {
                LinkedHashToMultiTypeMap<String> line = iterator.next();
                String chromosomeName = line.get("CHROM");

                vcfGenome.registerChromosome(new MutableString(chromosomeName));

                pg.lightUpdate();

                count++;
                if (count > earlyBreak) {
                    System.err.println("Aborting because we already processed --process-max");
                    break;
                }
            }
            pg.done();
            vcfGenome.prepare();
            genome = vcfGenome;
            vcfGenome.save(getGenomeBasename(vcfFilename));
        }


        // read the sample names from TSV first line:
        String tsvOutputFilename = getTsvFilename(vcfFilename);
        TsvLineIterator tsvLineReader = new TsvLineIterator(tsvOutputFilename);
        Iterator<LinkedHashToMultiTypeMap<String>> iterator = tsvLineReader.iterator();
        ObjectArraySet<String> sampleIdFromTsv = new ObjectArraySet<String>();
        if (iterator.hasNext()) {
            LinkedHashToMultiTypeMap<String> next = iterator.next();
            for (String columnId : next.keySet()) {
                if (columnId.endsWith("[GT]")) {
                    sampleIdFromTsv.add(columnId.replace("[GT]", ""));
                }
            }

        }
        tsvLineReader.close();

        // read the tsv again:
        tsvLineReader = new TsvLineIterator(tsvOutputFilename);
        iterator = tsvLineReader.iterator();
        genome.prepare();
        GtfAnnotationsI annotations = useBioJavaGtf ? new BioJavaGtfAnnotations(annotationFilename) :
                new GobyAnnotations(setupParser(annotationFilename), annotationFilename, genome, transcriptSelection);
        if (annotations.isEmpty()) {
            System.err.println("Error: Parameters have resulted in empty overlap between annotations and transcript selection.");
            System.exit(1);
        }
        String samples[] = sampleIdFromTsv.toArray(new String[sampleIdFromTsv.size()]);
        reconciliatePairInfoWithSamples(pairs, samples);
        AlloGenomicsScores scores = new AlloGenomicsScores(typeOfScore);
        scores.setOnlyHomozygoteAlleles(onlyHomozygousAlleles);
        scores.setTypeOfScore(typeOfScore);
        ProgressLogger pg = new ProgressLogger();
        pg.displayFreeMemory = true;
        pg.start("Starting to iterate VCF genomeFile");
        pg.itemsName = "sites";
        pg.expectedUpdates = numVcfItems;
        Collection<TransplantPair> focusPairs = clinical ? pairs.getClinicalPairs() : pairs.getAllTheoreticalPairs(maxTheoreticalPairs);
        focusPairs = filter(samples, focusPairs);
        System.out.printf("Calculating scores for %d %s pairs %n", focusPairs.size(), clinical ? "clinical" : "theoretical");

        ObjectSet<CurrentFrame> currentAnnotatedFrames = new ObjectArraySet<CurrentFrame>();
        ObjectSet<String> currentTranscripts = new ObjectArraySet<String>();
        String genotypeFieldName = "GT";
        String zygosityFieldName = "Zygosity";
        int excludedByAnnotationCount = 0;
        int excludedByTranscriptSelection = 0;
        if (allSites && transcriptSelectionFilename!=null) {
            System.err.println("The --all-sites option is not compatible with the -t option.");
            System.exit(1);
        }
        int count = 0;
        PrintWriter measuredSitesWriter=measuredSiteFilename==null?null: new PrintWriter(new FileWriter(measuredSiteFilename));
        while (iterator.hasNext()) {
            LinkedHashToMultiTypeMap<String> line = iterator.next();
            String chromosomeName = line.get("CHROM");
            boolean siteWasMeasured=false;
            String positionStr = line.get("POS");
            int position = Integer.parseInt(positionStr);
            annotations.getOverlappingTranscriptIds(currentAnnotatedFrames, chromosomeName, position, position + 1);
            currentTranscripts.clear();
            for (CurrentFrame cf : currentAnnotatedFrames) {
                currentTranscripts.add(cf.transcriptId);
            }
            if (allSites || currentAnnotatedFrames.size() > 0) {
                //        System.out.printf("annotation chromosome: %s vcf chr: %s %n",currentAnnotatedFrames.iterator().next().chromosome, chromosomeName);
                if (transcriptSelection.containsAnyTranscriptId(currentTranscripts)) {
                    boolean isIndel = "INDEL".equals(line.get("INDEL"));
                    if (considerIndels && isIndel || !isIndel) {
                        boolean isNonSynonymousVariation = nonSynonymousCoding(line.get(getEffectColumnName()));

                        final CharSequence ref = line.get("REF");
                        final CharSequence alt = line.get("ALT");

                        for (TransplantPair pair : focusPairs) {
                            String vcfRecipientSampleId1 = dnaSampleIdToVcfId.get(pair.getRecipientDNA().sampleId);
                            String vcfDonorSampleId2 = dnaSampleIdToVcfId.get(pair.getDonorDNA().sampleId);

                            String genotypeRecipientValueId1 = line.get(vcfRecipientSampleId1 + "[GT]");
                            String genotypeDonorValueId2 = line.get(vcfDonorSampleId2 + "[GT]");
                            String donorGoodBaseCount = line.get(vcfDonorSampleId2 + "[GB]");
                            String recipientGoodBaseCount = line.get(vcfRecipientSampleId1 + "[GB]");
                            int gb = Math.max(Integer.parseInt(donorGoodBaseCount), Integer.parseInt(recipientGoodBaseCount));

                            if (gb > maxDepthPerExome) {
                                continue;
                            }
                            if (!bothTyped(line, vcfRecipientSampleId1, vcfDonorSampleId2)) continue;

                            if (!genotypeRecipientValueId1.equals(genotypeDonorValueId2)) {
                                if (isNonSynonymousVariation) {
                                    scores.observeGenotypeMismatch(ref, alt, genotypeDonorValueId2, genotypeRecipientValueId1,
                                            pair, currentAnnotatedFrames);
                                    siteWasMeasured=true;
                                } else {
                                    scores.observeSynonymous(genotypeDonorValueId2,
                                            genotypeRecipientValueId1,
                                            pair, currentAnnotatedFrames);
                                }

                            }
                        }
                    }
                    if (siteWasMeasured && measuredSitesWriter!=null) {
                        measuredSitesWriter.printf("%s\t%d%n",chromosomeName, position);
                    }
                } else {
                    excludedByTranscriptSelection++;
                }
            } else {
                excludedByAnnotationCount++;
            }
            pg.lightUpdate();

            if (count++ > earlyBreak) {
                System.err.println("Aborting because we already processed --process-max-sites");
                break;
            }

        }
        pg.done();
        System.err.printf("%d sites were excluded by annotations (-a).%n", excludedByAnnotationCount);
    //    System.err.printf("%d sites were excluded by transcript selection (-t).%n", excludedByTranscriptSelection);
        switch (outputFormat) {
            case TSV:
                scores.printTSV(output);
                break;
            case BDVAL:
                scores.printBDVal(output);
                break;
            case PER_ANNOTATION:
                scores.printPerAnnotation(output);
                break;
        }
        if (measuredSitesWriter!=null ) {
            measuredSitesWriter.close();
        }
    }

    private AnnotationParser setupParser(String gtfAnnotationsFilename) {
        if (gtfAnnotationsFilename.endsWith(".gtf")) {
            return new GtfAnnotationParser();
        } else if (gtfAnnotationsFilename.endsWith("-ilmn.tsv")) {
            return new IlluminaAnnotationParser();
        }
        throw new IllegalArgumentException("The annotation file format could not be recognized. The file format is recognized by the filename extension. Valid extensions are .gtf and -ilmn.tsv");
    }

    private String getGenomeBasename(String vcfFilename) {
        return FilenameUtils.removeExtension(FilenameUtils.removeExtension(vcfFilename)) + "-genome";
    }


    private String convertVcfToTsv(VCFParser parser, String vcfFilename, int numVcfItems) throws IOException {
        String filename = FilenameUtils.removeExtension(vcfFilename);
        String tsvOutputFilename = getTsvFilename(filename);
        if (FileUtils.isFileNewer(new File(tsvOutputFilename), new File(vcfFilename))) {
            // when a file was already converted, return
            System.err.println("Using cached TSV file: " + tsvOutputFilename);
            return tsvOutputFilename;
        }
        PrintWriter writer = new PrintWriter(new GZIPOutputStream(new FileOutputStream(tsvOutputFilename)));
        final int chromosomeColumnIndex = parser.getGlobalFieldIndex("CHROM", "VALUE");
        final int positionColumnIndex = parser.getGlobalFieldIndex("POS", "VALUE");
        final int refColumnIndex = parser.getGlobalFieldIndex("REF", "VALUE");
        final int altColumnIndex = parser.getGlobalFieldIndex("ALT", "VALUE");
        final int effectColumnIndex = getVcfEffectColumnIndex(parser);
        final int indelFlagColumnIndex = parser.getGlobalFieldIndex("INFO", "INDEL");

        TSVWriter tsvWriter = new TSVWriter(writer);
        int tsvChromIndex = tsvWriter.defineColumn("CHROM");
        int tsvPosIndex = tsvWriter.defineColumn("POS");
        int tsvRefIndex = tsvWriter.defineColumn("REF");
        int tsvAltIndex = tsvWriter.defineColumn("ALT");
        int tsvIndelIndex = tsvWriter.defineColumn("INDEL");
        int tsvEffectIndex = tsvWriter.defineColumn(getEffectColumnName());
        final String[] samples = parser.getColumnNamesUsingFormat();
        int[] tsvSampleGenotypeIndices = new int[samples.length];
        int[] tsvSampleGBIndices = new int[samples.length];
        int[] tsvSampleZygozityIndices = new int[samples.length];
        int sampleIndex = 0;
        for (String sample : samples) {
            tsvSampleGenotypeIndices[sampleIndex] = tsvWriter.defineColumn(sample + "[GT]");
            tsvSampleGBIndices[sampleIndex] = tsvWriter.defineColumn(sample + "[GB]");
            tsvSampleZygozityIndices[sampleIndex] = tsvWriter.defineColumn(sample + "[Zygozity]");
            sampleIndex++;
        }
        ProgressLogger pg = new ProgressLogger();
        pg.displayFreeMemory = true;
        pg.start("Starting to convert VCF to TSV..");
        pg.itemsName = "sites";
        pg.expectedUpdates = numVcfItems;
        tsvWriter.writeHeader();
        while (parser.hasNextDataLine()) {

            String chromosomeName = parser.getColumnValue(chromosomeColumnIndex).toString();
            String positionStr = parser.getColumnValue(positionColumnIndex).toString();
            final CharSequence ref = parser.getColumnValue(refColumnIndex);
            final CharSequence alt = parser.getColumnValue(altColumnIndex);
            final CharSequence effect = parser.getFieldValue(effectColumnIndex);
            tsvWriter.setValue(tsvChromIndex, chromosomeName);
            tsvWriter.setValue(tsvPosIndex, positionStr);
            tsvWriter.setValue(tsvRefIndex, ref);
            tsvWriter.setValue(tsvAltIndex, alt);
            boolean isIndel = indelFlagColumnIndex != -1 && indelMutableString.equals(parser.getFieldValue(indelFlagColumnIndex));
            tsvWriter.setValue(tsvIndelIndex, isIndel ? "INDEL" : "NO");

            tsvWriter.setValue(tsvEffectIndex, effect);

            sampleIndex = 0;
            for (String sample : samples) {
                int globalFieldIndexSample = parser.getGlobalFieldIndex(sample, "GT");
                int globalFieldGB = parser.getGlobalFieldIndex(sample, "GB");
                int globalFieldZygozity = parser.getGlobalFieldIndex(sample, "Zygosity");
                String genotypeValue = parser.getStringFieldValue(globalFieldIndexSample);
                String gb = parser.getStringFieldValue(globalFieldGB);
                String zygozity = globalFieldZygozity == -1 ? "" : parser.getStringFieldValue(globalFieldZygozity);
                tsvWriter.setValue(tsvSampleGenotypeIndices[sampleIndex], genotypeValue);
                tsvWriter.setValue(tsvSampleGBIndices[sampleIndex], gb);
                tsvWriter.setValue(tsvSampleZygozityIndices[sampleIndex], zygozity);
                sampleIndex++;
            }
            tsvWriter.writeRecord();
            pg.lightUpdate();
            parser.next();
        }
        pg.done();
        tsvWriter.close();
        return tsvOutputFilename;
    }

    private int getVcfEffectColumnIndex(VCFParser parser) {
        int index = parser.getGlobalFieldIndex("INFO", getEffectColumnName());
        int csqIndex = parser.getGlobalFieldIndex("INFO", "CSQ");
        if (index == -1 && useVepTerms && csqIndex != -1) {
            return csqIndex;
        } else
            return index;
    }

    private String getTsvFilename(String filename) {
        String tsvOutputFilename = filename;
        for (String extension : new String[]{"gz", "vcf"}) {
            if (FilenameUtils.isExtension(tsvOutputFilename, extension)) {
                tsvOutputFilename = FilenameUtils.removeExtension(tsvOutputFilename);
            }
        }
        tsvOutputFilename = tsvOutputFilename + ".tsv.gz";
        return tsvOutputFilename;
    }

    // keep only pairs whose samples are both present in the VCF file.
    private Collection<TransplantPair> filter(String[] samples, Collection<TransplantPair> focusPairs) {

        ObjectArrayList<String> sampleSet = ObjectArrayList.wrap(samples);
        ObjectArrayList<TransplantPair> removeSet = new ObjectArrayList<TransplantPair>();
        for (TransplantPair pair : focusPairs) {

            if (!sampleSet.contains(dnaSampleIdToVcfId.get(pair.getDonorDNA().sampleId)) ||
                    !sampleSet.contains(dnaSampleIdToVcfId.get(pair.getRecipientDNA().sampleId))) {

                removeSet.add(pair);
            }
        }
        focusPairs.removeAll(removeSet);
        return focusPairs;
    }

    public void setProcessMax(int processMax) {
        this.processMax = processMax;
    }

    public int getProcessMax() {
        return processMax;
    }

    public void setOnlyHomozygousAlleles(boolean onlyHomozygousAlleles) {
        this.onlyHomozygousAlleles = onlyHomozygousAlleles;
    }


    public boolean isNoDash() {
        return noDash;
    }

    enum OutputFormat {
        TSV,
        BDVAL,
        PER_ANNOTATION
    }

    OutputFormat outputFormat = OutputFormat.TSV;

    public void setOutputFormat(OutputFormat outputFormat) {
        this.outputFormat = outputFormat;
    }

    private ObjectArraySet<String> vepTerms = new ObjectArraySet<String>();

    {
        // initiator_codon_variant	A codon variant that changes at least one base of the first codon of a transcript	SO:0001582	Non synonymous coding
        // inframe_insertion	An inframe non synonymous variant that inserts bases into in the coding sequence	SO:0001821
        // inframe_deletion	An inframe non synonymous variant that deletes bases from the coding sequence	SO:0001822
        // missense_variant	A sequence variant, that changes one or more bases, resulting in a different amino acid sequence but where the length is preserved	SO:0001583
        vepTerms.add("initiator_codon_variant");
        vepTerms.add("inframe_insertion");
        vepTerms.add("inframe_deletion");
        vepTerms.add("missense_variant");
        vepTerms.add("frameshift_variant");
        vepTerms.add("stop_gained");
        vepTerms.add("stop_lost");
        vepTerms.add("splice_donor_variant");
        vepTerms.add("splice_acceptor_variant");
    }

    private int infoEffectIndex = -1;
    private int vepEffectIndex = -1;

    public String getEffectColumnName() {
        if (useEnsemblTerms) {
            return "Effect";
        }
        if (useVepTerms) {
            return "VariantEffectPrediction";

        }
        return null;
    }

    private boolean nonSynonymousCoding(String effect) {
        if (!filterNonSynonymousCoding) return true;
        if (effect == null || effect.isEmpty()) return false;
        if (useEnsemblTerms) {

            return effect.contains("NON_SYNONYMOUS_CODING");
        }

        if (useVepTerms) {

            for (String vepTerm : vepTerms) {

                if (effect.contains(vepTerm)) {
                    return true;
                }
            }
            return false;
        }
        assert false : "At least one of useEnsemblTerms or useVepTerms must be active.";
        return false;
    }

    private boolean nonSynonymousCoding(VCFParser parser) {

        if (!filterNonSynonymousCoding) return true;

        if (useEnsemblTerms) {
            if (infoEffectIndex == -1) {
                infoEffectIndex = parser.getGlobalFieldIndex("INFO", "Effect");
            }
            String infoEffect = parser.getStringFieldValue(infoEffectIndex);
            return infoEffect.contains("NON_SYNONYMOUS_CODING");
        }

        if (useVepTerms) {
            if (vepEffectIndex == -1) {
                vepEffectIndex = parser.getGlobalFieldIndex("INFO", "VariantEffectPrediction");
            }
            String infoEffect = parser.getStringFieldValue(vepEffectIndex);
            assert vepTerms.size() > 0 : "no vep terms to search";
            for (String vepTerm : vepTerms) {

                if (infoEffect.contains(vepTerm)) {
                    return true;
                }
            }
            return false;
        }
        assert false : "At least one of useEnsemblTerms or useVepTerms must be active.";
        return false;
    }

    private boolean bothTyped(LinkedHashToMultiTypeMap<String> line, String vcfRecipientSampleId1, String vcfDonorSampleId2) {

        String zygosityRecipient = line.get(vcfRecipientSampleId1 + "[Zygozity]");
        String zygosityDonor = line.get(vcfDonorSampleId2 + "[Zygozity]");
        int depthRecipient = Integer.parseInt(line.get(vcfRecipientSampleId1 + "[GB]"));
        int depthDonor = Integer.parseInt(line.get(vcfDonorSampleId2 + "[GB]"));
        return !(depthDonor < depthThreshold || depthRecipient < depthThreshold)
                && !("not-typed".equals(zygosityRecipient) || "not-typed".equals(zygosityDonor));
    }

    private boolean bothTyped(VCFParser parser, String vcfRecipientSampleId1, String vcfDonorSampleId2) {
        int globalFieldIndexRecipientId1 = parser.getGlobalFieldIndex(vcfRecipientSampleId1, "Zygosity");
        int globalFieldIndexDonorId1 = parser.getGlobalFieldIndex(vcfDonorSampleId2, "Zygosity");
        if (globalFieldIndexDonorId1 == -1 && globalFieldIndexRecipientId1 == -1) {
            // Zygosity field missing, assume all typed.
            return true;
        }
        String zygosityRecipient = parser.getStringFieldValue(globalFieldIndexRecipientId1);
        String zygosityDonor = parser.getStringFieldValue(globalFieldIndexDonorId1);
        int globalFieldIndexGoodBasesRecipient = parser.getGlobalFieldIndex(vcfRecipientSampleId1, "GB");
        int globalFieldIndexGoodBasesDonor = parser.getGlobalFieldIndex(vcfDonorSampleId2, "GB");
        int depthRecipient = Integer.parseInt(parser.getStringFieldValue(globalFieldIndexGoodBasesRecipient));
        int depthDonor = Integer.parseInt(parser.getStringFieldValue(globalFieldIndexGoodBasesRecipient));
        if (depthDonor < depthThreshold || depthRecipient < depthThreshold) {
            return false;
        }
        return !("not-typed".equals(zygosityRecipient) || "not-typed".equals(zygosityDonor));
    }

    private boolean dashPrefixPostfix = true;

    public void setNoDash(boolean noDash) {
        this.dashPrefixPostfix = !noDash;
    }

    private void reconciliatePairInfoWithSamples(TransplantPairs pairs, String[] vcfSamples) {
        for (String vcfSample : vcfSamples) {
            for (String pairSampleId : pairs.getAllSampleIds()) {
                final String match = (dashPrefixPostfix ? "-" : "") + pairSampleId + (dashPrefixPostfix ? "-" : "");
                if (vcfSample.contains(match)) {
                    vcfIdToDnaSampleId.put(vcfSample, pairSampleId);
                    dnaSampleIdToVcfId.put(pairSampleId, vcfSample);

                }

            }

        }
    }


    public void setTranscriptSelectionFilename(String transcriptSelectionFilename) {
        this.transcriptSelectionFilename = transcriptSelectionFilename;
    }

    public String getTranscriptSelectionFilename() {
        return transcriptSelectionFilename;
    }

    public void setClinical(boolean clinical) {
        this.clinical = clinical;
    }

    public boolean isClinical() {
        return clinical;
    }

    public void setOutput(PrintStream output) {
        this.output = output;
    }

    public PrintStream getOutput() {
        return output;
    }


    public void setMaxTheoreticalPairs(int maxTheoreticalPairs) {
        this.maxTheoreticalPairs = maxTheoreticalPairs;
    }

    public int getMaxTheoreticalPairs() {
        return maxTheoreticalPairs;
    }

    public void setFilterNonSynonymousCoding(boolean filterNonSynonymousCoding) {
        this.filterNonSynonymousCoding = filterNonSynonymousCoding;
    }
}
