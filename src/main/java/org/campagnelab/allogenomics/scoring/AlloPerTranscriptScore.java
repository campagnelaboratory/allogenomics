package org.campagnelab.allogenomics.scoring;

import it.unimi.dsi.fastutil.ints.Int2ObjectAVLTreeMap;
import it.unimi.dsi.fastutil.objects.Object2IntAVLTreeMap;
import it.unimi.dsi.fastutil.objects.Object2IntMap;
import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import it.unimi.dsi.fastutil.objects.ObjectSet;
import it.unimi.dsi.lang.MutableString;
import org.campagnelab.allogenomics.TransplantPair;
import org.campagnelab.allogenomics.annotations.CurrentFrame;

import java.util.Collection;
import java.util.Set;

/**
 * An individual score for each selected transcripts.
 *
 * @author Fabien Campagne
 *         Date: 2/16/13
 *         Time: 11:37 AM
 */
public class AlloPerTranscriptScore implements AlloScoreI {
    Object2IntMap<String> numGenotypeMismatches;
    Object2IntMap<String> numSynonymousVariationMismatches;

    public AlloPerTranscriptScore() {
        this.numGenotypeMismatches = new Object2IntAVLTreeMap<String>();
        numGenotypeMismatches.defaultReturnValue(0);
        this.numSynonymousVariationMismatches = new Object2IntAVLTreeMap<String>();
        numSynonymousVariationMismatches.defaultReturnValue(0);


    }

    public void observeGenotypeMismatch(ObjectSet<CurrentFrame> currentTranscripts, TransplantPair pair) {
        for (CurrentFrame cf : currentTranscripts) {
            int count = numGenotypeMismatches.getInt(cf.transcriptId);
            numGenotypeMismatches.put(cf.transcriptId, count + 1);
        }

    }

    public void observeSynonymousVariationMismatches(Set<CurrentFrame> currentTranscripts, TransplantPair pair) {

        for (CurrentFrame cf : currentTranscripts) {
            int count = numSynonymousVariationMismatches.getInt(cf.transcriptId);
            numSynonymousVariationMismatches.put(cf.transcriptId, count + 1);
        }
    }

    public String header() {
        return "transcriptId\tallo-score\tsynonymous-mismatches";
    }

    public String toString() {
        MutableString sb = new MutableString();
        for (String transcriptId : numGenotypeMismatches.keySet()) {
            int count = numGenotypeMismatches.getInt(transcriptId);
            int synonymousCount = numSynonymousVariationMismatches.getInt(transcriptId);

            sb.append(String.format("%s\t%d\t%d", transcriptId, count, synonymousCount));
        }
        return sb.toString();

    }

    private Int2ObjectAVLTreeMap<String> indices;

    public int numItems() {
        indices = new Int2ObjectAVLTreeMap<String>();
        int index = 0;
        for (String transcriptId : numGenotypeMismatches.keySet()) {
            indices.put(index++, transcriptId);
        }
        return numGenotypeMismatches.size();
    }

    public String item(int index) {
        String transcriptId = indices.get(index);
        int count = numGenotypeMismatches.getInt(transcriptId);
        int synonymousCount = numSynonymousVariationMismatches.getInt(transcriptId);
        return String.format("%s\t%d\t%d", transcriptId, count, synonymousCount);
    }

    public Collection<String> featureIds() {

        return numGenotypeMismatches.keySet();

    }


    public int getScore(String transcriptId) {
        return numGenotypeMismatches.getInt(transcriptId);
    }
    public int getSynonymous(String transcriptId) {
        return numSynonymousVariationMismatches.getInt(transcriptId);
    }

}
