package org.campagnelab.allogenomics.scoring;

import it.unimi.dsi.fastutil.objects.ObjectSet;
import org.campagnelab.allogenomics.TransplantPair;
import org.campagnelab.allogenomics.annotations.CurrentFrame;

import java.util.Collection;
import java.util.Set;

/**
 * @author Fabien Campagne
 *         Date: 2/18/13
 *         Time: 11:59 AM
 */
public interface AlloScoreI {
    void observeGenotypeMismatch(ObjectSet<CurrentFrame> currentTranscripts, TransplantPair pair);

    void observeSynonymousVariationMismatches(Set<CurrentFrame> currentFrames, TransplantPair pair);
    /**
     * String for header (must not include new line).
     * @return
     */
    String header();
    @Override
    String toString();

    int numItems();

    String item(int index);

    Collection<String> featureIds();


}
