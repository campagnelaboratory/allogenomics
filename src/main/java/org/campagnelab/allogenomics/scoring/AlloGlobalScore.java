package org.campagnelab.allogenomics.scoring;

import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import it.unimi.dsi.fastutil.objects.ObjectSet;
import org.campagnelab.allogenomics.TransplantPair;
import org.campagnelab.allogenomics.annotations.CurrentFrame;

import java.util.Collection;
import java.util.Set;

/**
 * A global score across all selected transcripts.
 *
 * @author Fabien Campagne
 *         Date: 2/16/13
 *         Time: 11:37 AM
 */
public class AlloGlobalScore implements AlloScoreI {
    int numGenotypeMismatches = 0;
    int numSynonymousVariationMismatches=0;
    public void observeGenotypeMismatch(ObjectSet<CurrentFrame> currentTranscripts, TransplantPair pair) {
        numGenotypeMismatches++;
    }

    public void observeSynonymousVariationMismatches(Set<CurrentFrame> currentFrames, TransplantPair pair) {
        numSynonymousVariationMismatches+=1;
    }
    public String header() {
        return "allo-score\tsynonymous-mismatches";
    }

    public String toString() {
        return String.format("%d\t%d", numGenotypeMismatches,numSynonymousVariationMismatches);
    }

    public int numItems() {
        return 1;
    }

    public String item(int index) {
        return toString();
    }

    public Collection<String> featureIds() {
        Collection<String> result=new ObjectArrayList<String>();
        result.add("score");
        return result;
    }

}
