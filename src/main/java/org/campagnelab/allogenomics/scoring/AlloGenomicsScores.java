package org.campagnelab.allogenomics.scoring;

import edu.cornell.med.icb.identifier.IndexedIdentifier;
import it.unimi.dsi.fastutil.bytes.ByteArraySet;
import it.unimi.dsi.fastutil.bytes.ByteOpenHashSet;
import it.unimi.dsi.fastutil.bytes.ByteSet;
import it.unimi.dsi.fastutil.objects.*;
import it.unimi.dsi.lang.MutableString;
import org.campagnelab.allogenomics.TransplantPair;
import org.campagnelab.allogenomics.annotations.CurrentFrame;

import java.io.PrintStream;
import java.util.Arrays;
import java.util.Collections;
import java.util.Map;

/**
 * @author Fabien Campagne
 *         Date: 2/16/13
 *         Time: 11:35 AM
 */
public class AlloGenomicsScores {
    // the following stores the allo score for each recipient/donor pair:
    private Object2ObjectMap<TransplantPair, AlloScoreI> patientScores;
    private boolean onlyHomozygoteAlleles;

    public AlloGenomicsScores(TypeOfScore typeOfScore) {
        this.patientScores = new Object2ObjectAVLTreeMap<TransplantPair, AlloScoreI>();
        patientScores.defaultReturnValue(null);
        this.typeOfScore = typeOfScore;

    }

    public int observeGenotypeMismatch(CharSequence ref, CharSequence alt, String donorGenotype, String recipientGenotype,
                                       TransplantPair pair,
                                       ObjectSet<CurrentFrame> currentFrames) {
        AlloScoreI score = null;
        if (pair != null) {
            score = patientScores.get(pair);
            if (score == null) {
                score = newAlloScore();
                patientScores.put(pair, score);
            }
        }
        donorAlleles.clear();
        recipientAlleles.clear();
        for (String allele : donorGenotype.split("[/|]")) {
            if (allele.equals(".")) {
                // ignore since donor is not typed here.
                return 0;
            }
            donorAlleles.add(Byte.parseByte(allele));
        }
        for (String allele : recipientGenotype.split("[/|]")) {
            if (allele.equals(".")) {
                // ignore since recipient is not typed here.
                return 0;
            }
            recipientAlleles.add(Byte.parseByte(allele));
        }
        int mismatch = 0;
        for (byte alleleCode : donorAlleles) {
            if (!recipientAlleles.contains(alleleCode)) {
                if (onlyHomozygoteAlleles) {
                    if (recipientAlleles.size() != 1 || donorAlleles.size() != 1) {
                        continue;
                    }
                }
                if (pair != null) {
                    score.observeGenotypeMismatch(currentFrames, pair);
                }
                mismatch += 1;
            }
        }
        return mismatch;
    }

    /**
     * Observe a synonymous variation.
     *
     * @param donorGenotype
     * @param recipientGenotype
     * @param pair
     * @param currentFrames
     */
    public int observeSynonymous(String donorGenotype, String recipientGenotype,
                                 TransplantPair pair, ObjectSet<CurrentFrame> currentFrames) {
        AlloScoreI score = null;
        if (pair != null) {
            score = patientScores.get(pair);
            if (score == null) {
                score = newAlloScore();
                patientScores.put(pair, score);
            }
        }
        donorAlleles.clear();
        recipientAlleles.clear();
        commonAlleles.clear();
        for (String allele : donorGenotype.split("[/|]")) {
            if (allele.equals(".")) {
                // ignore since donor is not typed here.
                return 0;
            }
            donorAlleles.add(Byte.parseByte(allele));
        }
        for (String allele : recipientGenotype.split("[/|]")) {
            if (allele.equals(".")) {
                // ignore since recipient is not typed here.
                return 0;
            }
            recipientAlleles.add(Byte.parseByte(allele));
        }
        if (onlyHomozygoteAlleles && (recipientAlleles.size() != 1 || donorAlleles.size() != 1)) {
            return 0;
        }
        commonAlleles.addAll(recipientAlleles);
        commonAlleles.retainAll(donorAlleles);
        int mismatches = 0;

        int numMismatches = Math.max(recipientAlleles.size(), donorAlleles.size()) -
                commonAlleles.size();
        for (int i = 0; i < numMismatches; i++) {
            if (pair != null) {
                score.observeSynonymousVariationMismatches(currentFrames, pair);
            }
            mismatches += 1;

        }
        return mismatches;
    }

    public void setOnlyHomozygoteAlleles(boolean onlyHomozygoteAlleles) {
        this.onlyHomozygoteAlleles = onlyHomozygoteAlleles;
    }


    public enum TypeOfScore {
        GLOBAL,
        PER_TRANSCRIPT
    }


    public void setTypeOfScore(TypeOfScore typeOfScore) {
        this.typeOfScore = typeOfScore;
    }

    private TypeOfScore typeOfScore;

    private AlloScoreI newAlloScore() {
        switch (typeOfScore) {
            case GLOBAL:
                return new AlloGlobalScore();

            case PER_TRANSCRIPT:
                return new AlloPerTranscriptScore();
            default:
                throw new UnsupportedOperationException("Not implemented.");

        }

    }

    ByteSet donorAlleles = new ByteArraySet();
    ByteSet recipientAlleles = new ByteArraySet();
    ByteSet commonAlleles = new ByteOpenHashSet();

    public void print(PrintStream out) {

        for (Map.Entry<TransplantPair, AlloScoreI> entry : patientScores.entrySet()) {
            TransplantPair pair = entry.getKey();
            AlloScoreI score = entry.getValue();
            out.printf(" pair: %s score: %s%n", pair, score);
        }
        out.flush();
    }

    public void printTSV(PrintStream out) {
        ObjectIterator<AlloScoreI> iterator = patientScores.values().iterator();
        if (iterator.hasNext()) {
            AlloScoreI aScore = iterator.next();
            out.printf("pair\t%s\tphenotype%n", aScore.header());
        }
        for (Map.Entry<TransplantPair, AlloScoreI> entry : patientScores.entrySet()) {
            TransplantPair pair = entry.getKey();
            AlloScoreI score = entry.getValue();
            for (int i = 0; i < score.numItems(); i++) {
                out.printf("%s\t%s\t%s%n", pair, score.item(i), pair.isClinical() ? pair.getPhenotype() : "");
            }
        }
        out.flush();
    }

    public void printPerAnnotation(PrintStream out) {
        ObjectList<TransplantPair> allPairIds = new ObjectArrayList<TransplantPair>();
        ObjectSet<String> allTranscriptIdsSet = new ObjectAVLTreeSet<String>();
        IndexedIdentifier phenotypes = new IndexedIdentifier();
        for (Map.Entry<TransplantPair, AlloScoreI> entry : patientScores.entrySet()) {
            TransplantPair pair = entry.getKey();
            allPairIds.add(pair);
            allTranscriptIdsSet.addAll(entry.getValue().featureIds());
            if (pair.getPhenotype() != null) {
                phenotypes.registerIdentifier(new MutableString(pair.getPhenotype() + "[allo]"));
                phenotypes.registerIdentifier(new MutableString(pair.getPhenotype() + "[synonymous]"));
            }
        }

        int scoresPerPhenotype[] = new int[phenotypes.size()];
        ObjectList<MutableString> phenotypesOrdered = new ObjectArrayList<MutableString>();

        phenotypesOrdered.addAll(phenotypes.keySet());
        Collections.sort(phenotypesOrdered);

        out.printf("transcriptId\t%s%n", joinStrings(phenotypesOrdered, "\t"));

        for (String transcriptId : allTranscriptIdsSet) {
            Arrays.fill(scoresPerPhenotype, 0);

            for (Map.Entry<TransplantPair, AlloScoreI> entry : patientScores.entrySet()) {
                TransplantPair pair = entry.getKey();
                String phenotype = pair.getPhenotype();
                if (phenotype != null) {
                    MutableString phenoAlloMut = new MutableString(phenotype + "[allo]");
                    scoresPerPhenotype[phenotypes.getInt(phenoAlloMut)] += getScore(pair, transcriptId);
                    MutableString phenoSynonymousMut = new MutableString(phenotype + "[synonymous]");
                    scoresPerPhenotype[phenotypes.getInt(phenoSynonymousMut)] += getSynonymousScore(pair, transcriptId);
                }
            }

            out.printf("%s\t", transcriptId);
            int last = allPairIds.size() * 2 - 1;
            int index = 0;
            for (int score : scoresPerPhenotype) {
                out.printf("%d", score);
                if (index++ != last) {
                    out.printf("\t");
                }
            }
            out.printf("%n");
        }
        out.flush();
    }

    public void printBDVal(PrintStream out) {

        ObjectList<TransplantPair> allPairIds = new ObjectArrayList<TransplantPair>();
        ObjectSet<String> allTranscriptIdsSet = new ObjectAVLTreeSet<String>();
        for (Map.Entry<TransplantPair, AlloScoreI> entry : patientScores.entrySet()) {
            allPairIds.add(entry.getKey());
            allTranscriptIdsSet.addAll(entry.getValue().featureIds());
        }
        Collections.sort(allPairIds);
        out.printf("ID_REF\t%s%n", joinPairs(allPairIds, "\t"));
        ObjectList<String> allTranscriptIds = new ObjectArrayList<String>();
        allTranscriptIds.addAll(allTranscriptIdsSet);
        for (String transcriptId : allTranscriptIds) {

            out.printf("%s\t", transcriptId);
            int last = allPairIds.size() - 1;
            int index = 0;
            for (TransplantPair pair : allPairIds) {
                int score = getScore(pair, transcriptId);
                out.printf("%d", score);
                if (index++ != last) {
                    out.printf("\t");
                }
            }
            out.printf("%n");
        }


        out.flush();
    }

    private int getScore(TransplantPair pair, String transcriptId) {
        AlloScoreI score = patientScores.get(pair);
        assert score instanceof AlloPerTranscriptScore : "BDVal format can only be used with PER_TRANSCRIPT scoring method.";
        return ((AlloPerTranscriptScore) score).getScore(transcriptId);
    }


    private int getSynonymousScore(TransplantPair pair, String transcriptId) {
        AlloScoreI score = patientScores.get(pair);
        assert score instanceof AlloPerTranscriptScore : "BDVal format can only be used with PER_TRANSCRIPT scoring method.";
        return ((AlloPerTranscriptScore) score).getSynonymous(transcriptId);
    }

    private String joinPairs(ObjectList<TransplantPair> allTranscriptIds, String delimiter) {
        StringBuffer sb = new StringBuffer();
        for (TransplantPair element : allTranscriptIds) {
            sb.append(element.toString());
            sb.append(delimiter);
        }
        return sb.substring(0, Math.max(0, sb.length() - 1));
    }

    private String joinStrings(ObjectList<MutableString> strings, String delimiter) {
        StringBuffer sb = new StringBuffer();
        for (CharSequence element : strings) {
            sb.append(String.format("score(%s)", element.toString()));
            sb.append(delimiter);
        }
        return sb.substring(0, Math.max(0, sb.length() - 1));
    }
}
