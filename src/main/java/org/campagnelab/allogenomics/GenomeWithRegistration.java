package org.campagnelab.allogenomics;

import edu.cornell.med.icb.goby.reads.RandomAccessSequenceInterface;
import it.unimi.dsi.lang.MutableString;

/**
 * @author Fabien Campagne
 *         Date: 6/21/14
 *         Time: 12:25 PM
 */
public interface GenomeWithRegistration extends RandomAccessSequenceInterface {
    /**
     * Register a new chromosome name.
     * @param chromosome
     */
    public void registerChromosome(MutableString chromosome);

    /**
     * Call this method after registring all the chromosome names and before using the genome for lookups.
     */
    public void prepare();

}
