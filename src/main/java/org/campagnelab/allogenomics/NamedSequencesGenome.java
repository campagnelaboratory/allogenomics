package org.campagnelab.allogenomics;

import edu.cornell.med.icb.goby.reads.RandomAccessSequenceInterface;
import edu.cornell.med.icb.identifier.DoubleIndexedIdentifier;
import edu.cornell.med.icb.identifier.IndexedIdentifier;
import it.unimi.dsi.fastutil.io.BinIO;
import it.unimi.dsi.lang.MutableString;

import java.io.IOException;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * This implementation of RandomAccessSequenceInterface only provide chromosome names and indices, not bases.
 *
 * @author Fabien Campagne
 *         Date: 2/17/13
 *         Time: 12:23 PM
 */
public class NamedSequencesGenome implements GenomeWithRegistration, Serializable {
    private IndexedIdentifier chromosomes = new IndexedIdentifier();
    private DoubleIndexedIdentifier reverseLookup;

    public void prepare() {
        reverseLookup = new DoubleIndexedIdentifier(chromosomes);
    }

    public void registerChromosome(MutableString chromosome) {
        chromosomes.registerIdentifier(chromosome);
    }


    public char get(int i, int i2) {
        throw new UnsupportedOperationException("This operation is not implemented.");
    }


    public int getLength(int i) {
        throw new UnsupportedOperationException("This operation is not implemented.");
    }


    public void getRange(int i, int i2, int i3, MutableString mutableString) {
        throw new UnsupportedOperationException("This operation is not implemented.");
    }


    public NamedSequencesGenome() {
        this.chromosomes.defaultReturnValue(-1);
    }

    public int getReferenceIndex(String s) {
        return chromosomes.getInt(new MutableString(s));
    }


    public String getReferenceName(int i) {
        MutableString id = reverseLookup.getId(i);
        if (id == null) {
            System.err.println("Could not find index=" + i);
            return "unknown";
        } else {
            return id.toString();
        }
    }

    public int size() {
        throw new UnsupportedOperationException("This operation is not implemented.");
    }

    public void save(String filename) {
        try {
            Map<MutableString, Integer> ids = new HashMap<MutableString, Integer>();
            for (MutableString chr : chromosomes.keySet()) {
                ids.put(chr, chromosomes.getInt(chr));
            }
            BinIO.storeObject(ids, filename);
            return;

        } catch (IOException e) {

            System.err.println("Cannot save VCF genome to filename: " + filename);
            throw new RuntimeException("Cannot save VCF genome to filename: " + filename, e);
        }
    }

    public static NamedSequencesGenome load(String filename) {
        try {
            NamedSequencesGenome instance = new NamedSequencesGenome();
            HashMap<MutableString, Integer> ids = (HashMap<MutableString, Integer>) BinIO.loadObject(filename);
            for (MutableString chr : ids.keySet()) {
                instance.chromosomes.put(chr, ids.get(chr));
            }
            instance.prepare();
            return instance;

        } catch (ClassNotFoundException e) {

        } catch (IOException e) {

        }
        System.err.println("Cannot load VCF genome from filename: " + filename);
        throw new RuntimeException("Cannot save VCF genome to filename: " + filename);
    }
}
