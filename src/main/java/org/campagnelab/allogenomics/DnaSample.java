package org.campagnelab.allogenomics;

/**
 * Represents a DNA sample used in a transplant pair.
 *
 * @author Fabien Campagne
 *         Date: 2/15/13
 *         Time: 6:20 PM
 */
public class DnaSample {
    String sampleId;
    String kind;

    @Override
    public String toString() {
        return kind+":"+sampleId;
    }
}
