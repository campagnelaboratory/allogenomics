package org.campagnelab.allogenomics;

import it.unimi.dsi.fastutil.objects.ObjectAVLTreeSet;
import it.unimi.dsi.fastutil.objects.ObjectSet;
import it.unimi.dsi.io.FastBufferedReader;
import it.unimi.dsi.io.LineIterator;
import it.unimi.dsi.lang.MutableString;

import java.io.FileNotFoundException;
import java.io.FileReader;

/**
 * Support selecting subsets of transcripts.
 *
 * @author Fabien Campagne
 *         Date: 2/17/13
 *         Time: 10:07 AM
 */
public class TranscriptSelection {
    ObjectSet<String> transcriptSet;
    private boolean acceptAllTranscript;

    public TranscriptSelection(String transcriptSelectionFilename) throws FileNotFoundException {
        if (transcriptSelectionFilename != null) {
            load(transcriptSelectionFilename);
            acceptAllTranscript = false;
        } else {
            acceptAllTranscript = true;
        }
    }

    private void load(String transcriptSelectionFilename) throws FileNotFoundException {
        transcriptSet = new ObjectAVLTreeSet<String>();

        LineIterator it = new LineIterator(new FastBufferedReader(new FileReader(transcriptSelectionFilename)));
        while (it.hasNext()) {
            MutableString line = it.next();
            transcriptSet.add(line.trim().toString());
        }
        System.out.printf("Loaded %d selection transcripts from %s%n",
                transcriptSet.size(),transcriptSelectionFilename);
        if (transcriptSet.size()==0) {
            System.err.println("Error: the transcript selection file cannot be empty. Accepting all transcripts.");
            acceptAllTranscript=true;
        }
    }

    public boolean containsTranscriptId(String transcriptId) {
        return acceptAllTranscript || transcriptSet.contains(transcriptId);
    }

    /**
     * Returns true if any of the current transcripts are selected.
     *
     * @param currentTranscripts
     * @return
     */
    public boolean containsAnyTranscriptId(ObjectSet<String> currentTranscripts) {
        if (acceptAllTranscript) {
            return true;
        }
        for (String transcriptId : currentTranscripts) {
            if (containsTranscriptId(transcriptId)) return true;
        }
        return false;
    }
}
