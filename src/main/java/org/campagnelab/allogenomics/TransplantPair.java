package org.campagnelab.allogenomics;

/**
 * Describes pairs of DNA samples that were or could be combined in a transplant. Clinical pairs will have
 * an associated phenotype, while theoretical pairs do not (they were not observed).
 *
 * @author Fabien Campagne
 *         Date: 2/15/13
 *         Time: 6:19 PM
 */
public class TransplantPair implements Comparable<TransplantPair> {
    private DnaSample recipientDNA;
    private DnaSample donorDNA;
    private boolean clinical;

    public boolean isClinical() {
        return clinical;
    }

    public void setClinical(boolean clinical) {
        this.clinical = clinical;
    }

    /**
     * Phenotype observed for this transplant pair. (e.g., ACR, or normal at one year following transplant).
     */
    private String phenotype;

    public String getPhenotype() {
        return phenotype;
    }

    public void setPhenotype(String phenotype) {
        this.phenotype = phenotype;
    }

    public DnaSample getRecipientDNA() {
        return recipientDNA;
    }

    public DnaSample getDonorDNA() {
        return donorDNA;
    }

    public TransplantPair(DnaSample lookupSample, DnaSample pairedSample) {
        assert !(lookupSample.kind).equalsIgnoreCase(pairedSample.kind) : "DNA kind must be different in a pair: " +
                lookupSample + " " + pairedSample;
        if ("Donor".equalsIgnoreCase(lookupSample.kind)) {
            donorDNA = lookupSample;
            recipientDNA = pairedSample;
        } else if ("Recipient".equalsIgnoreCase(lookupSample.kind)) {
            recipientDNA = lookupSample;
            donorDNA = pairedSample;
        }
    }

    @Override
    public int hashCode() {
        return   donorDNA.sampleId.hashCode() ^
                        recipientDNA.sampleId.hashCode();
    }

    @Override
    public boolean equals(Object o) {
        if (!(o instanceof TransplantPair)) {
            return false;
        }
        TransplantPair transplantPair = (TransplantPair) o;
        return   donorDNA.sampleId.equals(transplantPair.donorDNA.sampleId) &&
                recipientDNA.sampleId.equals(transplantPair.recipientDNA.sampleId);
    }

    @Override
    public String toString() {
        return String.format("%s donor:%s|recipient:%s >> %s", clinical ? "clinical" : "theoretical", donorDNA.sampleId, recipientDNA.sampleId, phenotype == null ? "?" : phenotype);
    }


    public int compareTo(TransplantPair transplantPair) {
        final int BEFORE = -1;
        final int EQUAL = 0;
        final int AFTER = 1;

        //this optimization is usually worthwhile, and can
        //always be added
        if (this == transplantPair) return EQUAL;
        //objects, including type-safe enums, follow this form
        //note that null objects will throw an exception here
        int comparison = recipientDNA.sampleId.compareTo(transplantPair.recipientDNA.sampleId);

        if (comparison != EQUAL) return comparison;
        comparison = donorDNA.sampleId.compareTo(transplantPair.donorDNA.sampleId);
        if (comparison != EQUAL) return comparison;


        //all comparisons have yielded equality
        //verify that compareTo is consistent with equals (optional)
        assert this.equals(transplantPair) : "compareTo inconsistent with equals.";
        return EQUAL;
    }
}
