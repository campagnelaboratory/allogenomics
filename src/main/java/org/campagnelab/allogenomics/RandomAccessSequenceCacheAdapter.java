package org.campagnelab.allogenomics;

import edu.cornell.med.icb.goby.reads.RandomAccessSequenceCache;
import it.unimi.dsi.lang.MutableString;

/**
 * @author Fabien Campagne
 *         Date: 6/21/14
 *         Time: 12:30 PM
 */
public class RandomAccessSequenceCacheAdapter extends RandomAccessSequenceCache implements GenomeWithRegistration {
    @Override
    public void registerChromosome(MutableString chromosome) {

    }

    @Override
    public void prepare() {

    }
}
