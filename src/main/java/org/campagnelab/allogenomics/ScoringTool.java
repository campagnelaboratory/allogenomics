package org.campagnelab.allogenomics;

import com.martiansoftware.jsap.JSAP;
import com.martiansoftware.jsap.JSAPResult;
import org.campagnelab.allogenomics.scoring.AlloGenomicsScores;

import java.io.FileOutputStream;
import java.io.PrintStream;

/**
 * Compute allogenomics scores for DNA transplant pairs.
 *
 * @author Fabien Campagne
 */
public class ScoringTool {
    public static void main(String[] args) throws Exception {
        JSAP jsap = new JSAP(ScoringTool.class.getResource("ScoringTool.jsap"));

        JSAPResult config = jsap.parse(args);

        if (!config.success() || config.getBoolean("help") || hasError(config)) {

            // print out the help, then specific error messages describing the problems
            // with the command line, THEN print usage.
            //  This is called "beating the user with a clue stick."

            System.err.println(jsap.getHelp());

            for (java.util.Iterator errs = config.getErrorMessageIterator();
                 errs.hasNext(); ) {
                System.err.println("Error: " + errs.next());
            }

            System.err.println();
            System.err.println("Usage: java "
                    + ScoringTool.class.getName());
            System.err.println("                "
                    + jsap.getUsage());
            System.err.println();

            System.exit(1);
        }
        String vcfFilename = config.getFile("input").getAbsolutePath();
        String phenotypeFilename = config.getFile("phenotype").getAbsolutePath();
        String gtfAnnotationsFilename = config.getFile("annotations").getAbsolutePath();
        String transcriptSelectionFilename = config.contains("transcript-selection") ? config.getFile("transcript-selection").getAbsolutePath() : null;
        int maxPairs = config.getInt("max-pairs");
        int depthThreshold = config.getInt("minimum-depth");
        int processMax = config.userSpecified("process-max-sites") ? config.getInt("process-max-sites") : Integer.MAX_VALUE;
        boolean filterNonSynonymousCoding = config.getBoolean("only-non-synonymous-coding");
        boolean onlyHomozygousAlleles = config.getBoolean("only-homozygous-alleles");
        VcfScorer.OutputFormat outputFormat = VcfScorer.OutputFormat.valueOf(config.getString("output-format").toUpperCase());
        int maxDepthPerExome = config.userSpecified("max-depth") ? config.getInt("max-depth") : Integer.MAX_VALUE;

        if (config.userSpecified("vep") && config.userSpecified("ensembl")) {
            System.err.println("You cannot use both --vep and --ensembl, please pick one. ");
            System.exit(1);
        }

        if (!(config.userSpecified("vep") || config.userSpecified("ensembl"))) {
            System.err.println("You must use either --vep or --ensembl. ");
            System.exit(1);
        }

        AlloGenomicsScores.TypeOfScore typeOfScore = AlloGenomicsScores.TypeOfScore.GLOBAL;
        switch (outputFormat) {
            case TSV:
                typeOfScore = AlloGenomicsScores.TypeOfScore.GLOBAL;
                break;
            case BDVAL:
            case PER_ANNOTATION:
                typeOfScore = AlloGenomicsScores.TypeOfScore.PER_TRANSCRIPT;
                break;
            default:
                System.err.println("Unable to determine type of score for output format: " + outputFormat);
                System.exit(1);
        }


        VcfScorer processor = new VcfScorer();
        if (transcriptSelectionFilename != null) {
            processor.setTranscriptSelectionFilename(transcriptSelectionFilename);
        }
        processor.setClinical(config.getBoolean("clinical"));
        processor.setConsiderIndels(config.getBoolean("consider-indels"));
        PrintStream output = config.contains("output") ? new PrintStream(new FileOutputStream(config.getString("output"))) : System.out;
        processor.setOutput(output);

        processor.setMaxTheoreticalPairs(maxPairs);
        processor.setTypeOfScore(typeOfScore);
        processor.setFilterNonSynonymousCoding(filterNonSynonymousCoding);
        processor.setDepthThreshold(depthThreshold);
        processor.setProcessMax(processMax);
        processor.setOutputFormat(outputFormat);
        processor.setMaxDepthPerExome(maxDepthPerExome);
        processor.setOnlyHomozygousAlleles(onlyHomozygousAlleles);
        processor.setNoDash(config.getBoolean("no-dash"));
        processor.setAllSites(config.getBoolean("all-sites"));
        processor.setMeasuredSiteFilename(config.getString("measured-sites"));
        if (config.userSpecified("vep")) {
            processor.setUseVepTerms(true);
        }
        if (config.userSpecified("ensembl")) {
            processor.setUseEnsemblTerms(true);
        }
        processor.process(vcfFilename, phenotypeFilename, gtfAnnotationsFilename);

        System.exit(0);
    }

    private static boolean hasError(JSAPResult config) {
        return false;
    }


}
