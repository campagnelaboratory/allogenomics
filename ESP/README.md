To extract maf from EVP database:


gunzip -c -d  ~/Downloads/ESP6500SI-V2-SSA137.protein-hgvs-update.snps_indels.txt.tar.gz  >/data/alloval1/ESP6500SI-V2-SSA137.tsv 





cut -d " " -f 1,8 /data/alloval1/ESP6500SI-V2-SSA137.tsv>/data/alloval1/ESP6500SI-pos-maf.txt



cat /data/alloval1/ESP6500SI-pos-maf.txt| awk 'BEGIN{FS="[: /]"; print "chromosome\tPOS\tmaf_EA\tmaf_AA\tmaf_All"} { if (NF>4) for (i=1;i<=NF;i++) printf("%s\t",$i); print ""}'  >/data/alloval1/ESP6500SI-pos-maf.tsv


