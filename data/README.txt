This directory contains sample data to demonstrate the use of ScoringTool. The following command line will process an
input VCF file to produce scores for each transplant pair:

-i input.vcf.gz -p pairs-definition.tsv -a protein_coding.gtf -t secreted+transmembrane.tsv -o secreted+transmembrane-scores.tsv


Some of these files were produced as described below:

1. Secreted proteins:  Got database from :http://spd.cbi.pku.edu.cn/sequence/spd-06/spd.06.fasta
 - Extracted Uniprot Accession codes:
 awk ' BEGIN{FS="[>\|]"} /^>/{ print $2} ' spd.06.fasta.txt | awk '{ for (i=1;i<=NF;i++) print $i}' >secreted-uniprot-acs.tsv

 - Got  transcript ids and uniprot acs from biomart:

2. Obtained transcript/exon structures from Ensembl:

 gzip -c -d Homo_sapiens.GRCh37.70.gtf.gz | awk '{if ($2=="protein_coding" && $3=="CDS") print $0}' >protein_coding.gtf